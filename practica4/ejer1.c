/*
* @file ejer1.c
* @author Pablo Medina Suarez
*
* Programa que cree N procesos hijos. Cada hijo debe compartir una variable 
* denominada contador, inicializada a 0. Debe ser incrementada por cada hilo
* 100 veces. Imprime la variable una vez finalizados los hilos y analice el 
* resultado obtenido.
*
*/
#include "shm_error.h"
#include <sys/shm.h>
#include <sys/wait.h>   // wait()
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
// Variables globales
int *contador = NULL;

// Funciones
void incrementar() {
	int i, l;

	for (i = 0; i < 100; i++) {
		l = *contador;
		l += 1;
		*contador = l;
	}
}

int main(int argc, char **argv) {
	// Comprueba numero de argumentos
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer1 num_procesos.\n");
		exit(EXIT_FAILURE);
	}

	int num_procesos = atoi(argv[1]), id_memoria, status, i;
	key_t clave;
	pid_t childpid, pid;

	// Comprueba numero de procesos adecuado
	if (num_procesos <= 0) {
		perror("Formato incorrecto. Introduzca un valor valido para num_procesos.\n");
		exit(EXIT_FAILURE);
	}

	// Obtencion de clave de memoria compartida
	if ((clave = ftok(argv[0], 10)) == -1) {
		ftokError();
	}

	// Reserva de memoria compartida
	if ((id_memoria = shmget(clave, sizeof(int), 0777 | IPC_CREAT)) == -1) {
		shmgetError();
	}

	// Mapeo de memoria
	if ((contador = (int *) shmat(id_memoria, (char *) 0,  0)) == NULL) {
		shmatError();
	}
	*contador = 0;

	// Creacion de procesos
	puts("Creando procesos...");
	for (i = 0; i < num_procesos; i++) {
		pid = fork();
		switch(pid) {
			case -1: 
				// Error
				perror("fork error");
            	printf("errno value= %d\n", errno);
				exit(EXIT_FAILURE);  
			case 0:
				// Proceso hijo
				printf("Proceso hijo %d. Padre = %d \n", getpid(), getppid());
				incrementar();
				exit(EXIT_SUCCESS);
		}
	}

	// Captura finalizacion de hijos
	for (i = 0; i < num_procesos; i++) {
		childpid = wait(&status);
		// Salida de hijo
		if (childpid > 0) {
	     	if (WIFEXITED(status)) {
	            // Salio con exito
	            printf("child %d exited successfully, status=%d\n",childpid, WEXITSTATUS(status));
	        } else if (WIFSIGNALED(status)) {
	            // Finalizado por otro proceso (kill)
	            printf("child %d killed (signal %d)\n", childpid, WTERMSIG(status));
	        } else if (WIFSTOPPED(status)) {
	            // Detenido por el sistema
	            printf("child %d stopped (signal %d)\n", childpid, WSTOPSIG(status));
	        } 
	    } else {
	        printf("Error en la invocacion de wait o la llamada ha sido interrumpida por una señal.\n");
	        exit(EXIT_FAILURE);
	    }
	}
	printf("Finalizacion de hilos. Resultado = %d.\n", *contador);

	// Finalizacion
	if ((shmdt((char *) contador)) == -1) {
		shmdtError();
	}
	if ((shmctl(id_memoria, IPC_RMID, (struct shmid_ds *) NULL)) == -1) {
		shmctlError();
	}
	exit(EXIT_SUCCESS);
}
