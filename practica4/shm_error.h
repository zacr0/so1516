/*
* @file shm_error.h
* @author Pablo Medina Suarez
*
* Declarcion de las funcoines de manejo de errores de la libreria shm
*/
void ftokError();
void shmgetError();
void shmctlError();
void shmatError();
void shmdtError();
