/*
* @file shm_error.c
* @author Pablo Medina Suarez
*
* Implementacion de las funciones declaradas en shm_error.h
*/
#include "shm_error.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

void ftokError() {
	switch(errno) {
		case EACCES:
			perror("Search permission is denied for a component of the path prefix.\n");
			exit(EXIT_FAILURE);
			break;
		case ELOOP:
			perror("A loop exists in symbolic links encountered during resolution of the path argument or More than {SYMLOOP_MAX} symbolic links were encountered during resolution of the path argument.\n");
			exit(EXIT_FAILURE);
			break;
		case ENAMETOOLONG:
			perror("The length of the path argument exceeds {PATH_MAX} or a pathname component is longer than {NAME_MAX} or Pathname resolution of a symbolic link produced an intermediate result whose length exceeds {PATH_MAX}.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOENT:
			perror("A component of path does not name an existing file or path is an empty string.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOTDIR:
			perror("A component of the path prefix is not a directory.\n");
			exit(EXIT_FAILURE);
			break;
		default:
			perror("Error ftok.\n");
			exit(EXIT_FAILURE);
			break;
	}
}

void shmgetError() {
	switch(errno) {
		case EACCES:
			perror("A shared memory identifier exists for key but operation permission as specified by the low-order nine bits of shmflg would not be granted; see XSI Interprocess Communication.\n");
			exit(EXIT_FAILURE);
			break;
		case EEXIST:
			perror("A shared memory identifier exists for the argument key but (shmflg &IPC_CREAT) &&(shmflg &IPC_EXCL) is non-zero.\n");
			exit(EXIT_FAILURE);
			break;
		case EINVAL:
			perror("A shared memory segment is to be created and the value of size is less than the system-imposed minimum or greater than the system-imposed maximum or No shared memory segment is to be created and a shared memory segment exists for key but the size of the segment associated with it is less than size and size is not 0.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOENT:
			perror("A shared memory identifier does not exist for the argument key and (shmflg &IPC_CREAT) is 0.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOMEM:
			perror("A shared memory identifier and associated shared memory segment shall be created, but the amount of available physical memory is not sufficient to fill the request.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOSPC:
			perror("A shared memory identifier is to be created, but the system-imposed limit on the maximum number of allowed shared memory identifiers system-wide would be exceeded.\n");
			exit(EXIT_FAILURE);
			break;
		default:
			perror("Error shmget.\n");
			exit(EXIT_FAILURE);
			break;
	}
}

void shmctlError() {
	switch(errno) {
		case EACCES:
			perror("The argument cmd is equal to IPC_STAT and the calling process does not have read permission; see XSI Interprocess Communication.\n");
			exit(EXIT_FAILURE);
			break;
		case EINVAL:
			perror("The value of shmid is not a valid shared memory identifier, or the value of cmd is not a valid command.\n");
			exit(EXIT_FAILURE);
			break;
		case EPERM:
			perror("The argument cmd is equal to IPC_RMID or IPC_SET and the effective user ID of the calling process is not equal to that of a process with appropriate privileges and it is not equal to the value of shm_perm.cuid or shm_perm.uid in the data structure associated with shmid.\n");
			exit(EXIT_FAILURE);
			break;
		case EOVERFLOW:
			perror("The cmd argument is IPC_STAT and the gid or uid value is too large to be stored in the structure pointed to by the buf argument.\n");
			exit(EXIT_FAILURE);
			break;
		default:
			perror("Error shmctl.\n");
			exit(EXIT_FAILURE);
			break;
	}
}

void shmatError() {
	switch(errno) {
		case EACCES:
			perror("Operation permission is denied to the calling process; see XSI Interprocess Communication.\n");
			exit(EXIT_FAILURE);
			break;
		case EINVAL:
			perror("The value of shmid is not a valid shared memory identifier, the shmaddr is not a null pointer, and the value of (shmaddr -((uintptr_t)shmaddr %SHMLBA)) is an illegal address for attaching shared memory; or the shmaddr is not a null pointer, (shmflg &SHM_RND) is 0, and the value of shmaddr is an illegal address for attaching shared memory.\n");
			exit(EXIT_FAILURE);
			break;
		case EMFILE:
			perror("The number of shared memory segments attached to the calling process would exceed the system-imposed limit.\n");
			exit(EXIT_FAILURE);
			break;
		case ENOMEM:
			perror("The available data space is not large enough to accommodate the shared memory segment.\n");
			exit(EXIT_FAILURE);
			break;
		default:
			perror("Error shmat.\n");
			exit(EXIT_FAILURE);
			break;
	}
}

void shmdtError() {
	switch(errno) {
		case EINVAL:
			perror("The value of shmaddr is not the data segment start address of a shared memory segment.");
			exit(EXIT_FAILURE);
			break;
		default:
			perror("Error shmdt.\n");
			exit(EXIT_FAILURE);
			break;
	}
}
