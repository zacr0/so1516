/*
* @file ejer2.c
* @author Pablo Medina Suarez
*
* Repetir el ejercicio 1 realizando la operacion dentro de una zona de
* exclusion mutua para evitar el problema de inconsistencia.
* Utilice semaforos generales tipo mutex.
*/
#include "shm_error.h"
#include "semaphore_error.h"
#include <sys/shm.h>
#include <sys/wait.h>   // wait()
#include <errno.h>
#include <unistd.h>
#include <semaphore.h>	// funciones sem_t
#include <stdio.h>
#include <stdlib.h>
// Variables globales
typedef struct params {
	int contador;
	sem_t mutex;	// exclusion mutua
} params;

int main(int argc, char **argv) {
	// Comprueba numero de argumentos
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer1 num_procesos.\n");
		exit(EXIT_FAILURE);
	}

	int num_procesos = atoi(argv[1]), id_memoria, status, i, l;
	key_t clave;
	pid_t childpid, pid;
	params *parametros = NULL;

	// Comprueba numero de procesos adecuado
	if (num_procesos <= 0) {
		perror("Formato incorrecto. Introduzca un valor valido para num_procesos.\n");
		exit(EXIT_FAILURE);
	}

	// Obtencion de clave de memoria compartida
	if ((clave = ftok(argv[0], 50)) == -1) {
		ftokError();
	}

	// Reserva de memoria compartida
	if ((id_memoria = shmget(clave, sizeof(parametros), 0777 | IPC_CREAT)) == -1) {
		shmgetError();
	}

	// Mapeo de memoria
	if ((parametros = (params *) shmat(id_memoria, (char *) 0,  0)) == NULL) {
		shmatError();
	}
	(*parametros).contador = 0;

	// Inicializacion de semaforo
	if (sem_init(&(*parametros).mutex, 1, 1)) {
		initSemError();
	}

	// Creacion de procesos
	puts("Creando procesos...");
	for (i = 0; i < num_procesos; i++) {
		pid = fork();
		switch(pid) {
			case -1: 
				// Error
				perror("fork error");
            	printf("errno value= %d\n", errno);
				exit(EXIT_FAILURE);  
			case 0:
				// Proceso hijo
				printf("Proceso hijo %d. Padre = %d \n", getpid(), getppid());
				// Inicio seccion critica
				if (sem_wait(&(*parametros).mutex)) {
					waitSemError();
				}

				for (i = 0; i < 100; i++) {
					l = (*parametros).contador;
					l += 1;
					(*parametros).contador = l;
				}

				if (sem_post(&(*parametros).mutex)) {
					postSemError();
				}
				// Fin seccion critica
				exit(EXIT_SUCCESS);
		}
	}

	// Captura finalizacion de hijos
	for (i = 0; i < num_procesos; i++) {
		childpid = wait(&status);
		// Salida de hijo
		if (childpid > 0) {
	     	if (WIFEXITED(status)) {
	            // Salio con exito
	            printf("child %d exited successfully, status=%d\n",childpid, WEXITSTATUS(status));
	        } else if (WIFSIGNALED(status)) {
	            // Finalizado por otro proceso (kill)
	            printf("child %d killed (signal %d)\n", childpid, WTERMSIG(status));
	        } else if (WIFSTOPPED(status)) {
	            // Detenido por el sistema
	            printf("child %d stopped (signal %d)\n", childpid, WSTOPSIG(status));
	        } 
	    } else {
	        printf("Error en la invocacion de wait o la llamada ha sido interrumpida por una señal.\n");
	        exit(EXIT_FAILURE);
	    }
	}
	printf("Finalizacion de hilos. Resultado = %d.\n", (*parametros).contador);

	// Finalizacion
	if ((shmdt((char *) parametros)) == -1) {
		shmdtError();
	}
	if ((shmctl(id_memoria, IPC_RMID, (struct shmid_ds *) NULL)) == -1) {
		shmctlError();
	}
	if (sem_destroy(&(*parametros).mutex)) {
		destroySemError();
	}
	exit(EXIT_SUCCESS);
}
