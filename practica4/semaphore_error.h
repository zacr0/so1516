/*
* @file semaphore_error.h
* @author Pablo Medina Suarez
*
* Declaracion de funciones para manejo de errores de la libreria semaphore.
*/
#ifndef SEMAPHORE_ERROR_H
#define SEMAPHORE_ERROR_H
void initSemError();
void waitSemError();
void postSemError();
void getValueSemError();
void destroySemError();
#endif
