/*
* @file ejer3.c
* @author Pablo Medina Suarez
*
* Productor-consumidor utilizando memoria compartida.
* Ejemplos en: https://see.stanford.edu/materials/icsppcs107/23-Concurrency-Examples.pdf
*/