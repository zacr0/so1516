#include <sys/types.h> //Para estructura pid_t 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Para fork()
#include <errno.h> // captura de errores 

int main(void) {
    pid_t pid;
    int status, childpid;
    
    pid = fork(); 
    switch(pid) {
        case -1:
            // Error
            perror("fork error");
            printf("errno value= %d\n", errno);
			exit(EXIT_FAILURE); //Necesaria la libreria <stdlib.h>  
        case 0: 
            // Proceso hijo
            printf("Proceso hijo %d; mi padre = %d \n", getpid(), getppid());
            exit(EXIT_SUCCESS);
        default: 
            // Proceso padre
            printf("Proceso padre %d; mi padre = %d \n", getpid(), getppid());
			/*Averigue quien es el padre del proceso padre*/

			//Se espera al hijo para evitar que quede como zombie
            childpid = wait(&status); 
            if (childpid > 0) {
             	if (WIFEXITED(status)) {
                    // Salio con exito
                    printf("child %d exited successfully, status=%d\n",childpid, WEXITSTATUS(status));
                } else if (WIFSIGNALED(status)) {
                    // Finalizado por otro proceso (kill)
                    printf("child %d killed (signal %d)\n", childpid, WTERMSIG(status));
                } else if (WIFSTOPPED(status)) {
                    // Detenido por el sistema
                    printf("child %d stopped (signal %d)\n", childpid, WSTOPSIG(status));
               } 
            } else {
                printf("Error en la invocacion de wait o la llamada ha sido interrumpida por una señal.\n");
                exit(EXIT_FAILURE);
            } 
            exit(EXIT_SUCCESS); //return 0;
    }
}
