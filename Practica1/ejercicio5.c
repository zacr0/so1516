/*
* @file ejercicio5.c
* @author Pablo Medina Suarez
*
* Procesos que realizan escritura simultanea sobre un mismo fichero.
*/
#include "io.h"         // Mensajes de error
#include <sys/types.h>  // estructura pid_t
#include <sys/wait.h>   // wait()
#include <stdio.h>		// I/O
#include <stdlib.h>
#include <unistd.h>     // fork()
#include <errno.h>		// captura de errores
// CONSTANTES
#define MAX_ESCRITURAS 20 // Escrituras hechas por cada proceso

int main(int argc, char **argv) {
	if (argc == 2) {
		// Numero de argumentos adecuado
		pid_t pid;
		int childpid, status, i;
		char *rutaFichero = argv[1], confirmacion;
		char *argumentos[3] = {"cat", rutaFichero, NULL};
		FILE *fich = NULL;

		// Apertura de fichero:
		if (existeFichero(rutaFichero)) {
			printf("El fichero %s ya existe. Se sobreescribira su contenido.\n", rutaFichero);
		} else {
			printf("El fichero %s no existe, se creara a continuacion.\n", rutaFichero);
		}

		if ((fich = fopen(rutaFichero, "w")) == NULL) {
			fprintf(stderr, "Error al abrir <%s>.\n", rutaFichero);
			exit(EXIT_FAILURE);
		}

		// Inicio proceso hijo:
		pid = fork();
		switch(pid) {
			case -1:
				// Error
				perror("fork error");
				printf("errno value= %d\n", errno); 
				exit(EXIT_FAILURE);
			case 0:
				// Hijo
				printf("Hijo de %d. PID = %d.\n", getppid(), getpid());

				// Escritura en fichero
				puts("Hijo. Comenzando escritura...");
				for (i = 0; i < MAX_ESCRITURAS; i++) {
					fputs("+++++++++++++\n", fich);
					sleep(1);
				}
				puts("Fin de escrituras del hijo.");

				exit(EXIT_SUCCESS);
			default:
				// Padre
				printf("Padre. PID = %d.\n", getpid());

				// Escritura en fichero
				puts("Padre. Comenzando escritura...");
				for (i = 0; i < MAX_ESCRITURAS; i++) {
					fputs("-------------\n", fich);
					sleep(1);
				}
				puts("Fin de escrituras del padre.");

				childpid = wait(&status);
				mostrarMsgFinHijo(childpid, status);

				// Cierre de fichero
				fclose(fich);

				// Visualizacion de fichero
				do {
					printf("¿Quiere ver el contenido de %s? (s/n)\n", rutaFichero);
					confirmacion = getchar();
					getchar();

					if (confirmacion != 's' && confirmacion != 'n') {
						puts("Introduzca 's' o 'n' para continuar.");
					}
				} while(confirmacion != 's' && confirmacion != 'n');

				if (confirmacion == 's') {
					if (execvp(argumentos[0], argumentos) > 0) {
						printf("cat iniciado. PID = %d\n", getpid());
					} else {
						perror("exec");
						printf("errno value = %d\n", errno);
						exit(EXIT_FAILURE);
					}
				}
				// Fin
				exit(EXIT_SUCCESS);
		}
	} else {
		// Numero de argumentos incorrecto
		perror("Formato incorrecto. Introduzca ./ejer5.exe nombre-del-fichero.\n");
		exit(EXIT_FAILURE);
	}
}
