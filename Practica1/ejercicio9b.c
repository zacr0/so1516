/*
* @file ejercicio9b.c
* @author Pablo Medina Suarez
*
* Programa que recibe el PID de ejer9a como argumento y le envia la señal 
* SIGUSR1 cada 3 segundos un total de 8 veces.
* Antes de finalizar, envia la señal SIGINT para indicarle que debe salir.
*/
#include <sys/types.h>  // estructura pid_t 
#include <signal.h>     // captura de alarmas
#include <stdio.h>      // entrada/salida
#include <stdlib.h>     // exit()
#include <unistd.h>     // POSIX

int main(int argc, char **argv) {
    if (argc == 2) {
        // Numero de argumentos adecuado
        int pid_prog1 = atoi(argv[1]), i;

        // Envia 8 SIGUSR1 signals al programa con pid_prog1
        printf("Enviando señales a %d cada 3 segundos.\n", pid_prog1);
        for (i = 0; i < 7; i++) {
            puts("Enviando señal.");
            kill(pid_prog1, SIGUSR1);
            sleep(3);   // signals cada 3 segundos
        }

        // Hace salir a prog1
        printf("Deteniendo proceso %d...\n", pid_prog1);
        kill(pid_prog1, SIGINT);

        exit(EXIT_SUCCESS);
    } else {
        // Numero de argumentos insuficiente
        perror("Formato incorrecto. Introduzca ./ejer9b.exe PID-ejer9a (entero).\n");
        exit(EXIT_FAILURE);
    }
}
