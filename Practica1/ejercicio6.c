/*
* @file ejercicio6.c
* @author Pablo Medina Suarez
*
* Modifica el ejercicio 1, por ejemplo, añadiendo una variable global y 
* comprobando su valor al final de la ejecucion.
*/
#include "io.h"
#include <sys/types.h>  // estructura pid_t 
#include <sys/wait.h>   // wait()
#include <stdio.h>      // I/O
#include <stdlib.h>
#include <unistd.h>     // fork()
#include <errno.h>      // captura de errores 
// VARIABLES GLOBALES
int test_valor = 0;

int main() {
    pid_t pid;
    int num_procesos = 0, status, childpid, i;
    
    // Lectura de numero de procesos
    do {
        printf("Numero de procesos a crear: ");
        scanf("%d", &num_procesos); 
        getchar();

        if (num_procesos < 1) {
            printf("Introduzca un numero de procesos correcto.\n");
        }
    } while (num_procesos < 1);

    // Creacion de hijos en abanico
    puts("Creando hijos...");
    for (i = 0; i < num_procesos; i++) {
        pid = fork();
        if (pid == 0) {
            // Incremento de variable global
            test_valor++;
            // Proceso hijo
            printf("Proceso hijo %d; mi padre = %d. Global = %d\n", getpid(), getppid(), test_valor);
            exit(EXIT_SUCCESS);
        }
    }

    // Captura de fin de ejecucion de procesos hijo
    for (i = 0; i < num_procesos; i++) {
        childpid = wait(&status);
        // Captura de salida
        mostrarMsgFinHijo(childpid, status);
    }
    // Muestra el valor inalterado en pantalla
    printf("Padre. Global = %d.\n", test_valor);
    exit(EXIT_SUCCESS);
}
