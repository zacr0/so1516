/*
* @file ejercicio3.c
* @author Pablo Medina Suarez
*
* Creacion de arbol de procesos, devolviendo suma de ultima cifra del pid.
*/
#include "io.h"         // Mensajes de error
#include <sys/types.h>  // estructura pid_t
#include <sys/wait.h>   // wait()
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>     // fork()
#include <errno.h>      // captura de errores

int main() {
	pid_t pid;
	int status, childpid, sum_codigos = 0;

	// Creacion de hijos
	// Primer hijo (sin nietos)
	pid = fork();
	if (pid == 0) {
        sum_codigos = getpid() % 10;
        printf("Primer hijo %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);
        exit(sum_codigos);
	}

	// Segundo hijo (con nietos)
	pid = fork();
	if (pid == 0) {
        // Creacion de nietos
        // Primer nieto (sin bisnietos)
        pid = fork();
        if (pid == 0) {
            sum_codigos = getpid() % 10;
            printf("Primer nieto %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);
            exit(sum_codigos);
        }

        // Segundo nieto (con bisnieto)
        pid = fork();
        if (pid == 0) {
            // Creacion de bisnieto
            pid = fork();
            if (pid == 0) {
                sum_codigos = getpid() % 10;
                printf("Primer bisnieto %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);
                exit(sum_codigos);
            }

            // "descansa" y espera al bisnieto
            sleep(5);
            childpid = wait(&status);
            mostrarMsgFinHijo(childpid, status);
            sum_codigos = status + (getpid() % 10); // suma de ultima cifra pid
            printf("Segundo nieto %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);
            exit(sum_codigos);
        }

        // "descansa" y espera a los nietos
        sleep(5);
        childpid = wait(&status);
        mostrarMsgFinHijo(childpid, status);
        sum_codigos = status + (getpid() % 10);
        childpid = wait(&status);
        sum_codigos += status;
        printf("Segundo hijo %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);
        exit(sum_codigos);
	}

	// "descansa" y espera a los hijos
	sleep(5);
	childpid = wait(&status);
    mostrarMsgFinHijo(childpid, status);
	sum_codigos = status + (getpid() % 10);
	childpid = wait(&status);
    mostrarMsgFinHijo(childpid, status);
	sum_codigos += status;
    printf("Proceso padre %d; mi padre = %d. Suma = %d \n", getpid(), getppid(), sum_codigos);

	exit(EXIT_SUCCESS);
}