/*
* @file ejercicio4.c
* @author Pablo Medina Suarez
*
* Ejecucion de programas
*/
#include "io.h"         // Mensajes de error
#include <sys/types.h>  // estructura pid_t
#include <sys/wait.h>   // wait()
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>     // fork()
#include <errno.h>      // captura de errores

void execProgramaSinArgumentos(char *arg) {
	//char *args_programa1[2] = {arg, NULL};
	//if (execvp(args_programa1[0], args_programa1) > 0) {
	if (execlp(arg, arg, NULL) > 0) {
		printf("%s iniciado. PID = %d\n", arg, getpid());
	} else {
		perror("exec");
		printf("errno value = %d\n", errno);
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char **argv) {
	if (argc >= 3) {
		// Numero de comandos adecuado
		pid_t pid;
		int status, childpid;
		
		printf("Padre. PID = %d.\n", getpid());

		// Inicio de procesos hijo:
		pid = fork();
		switch(pid) {
			case -1:
				// Error
				perror("fork error");
				printf("errno value= %d\n", errno); 
				exit(EXIT_FAILURE);
			case 0:
				// Ejecuta primer programa
				printf("Iniciando %s...\n", argv[1]);
				execProgramaSinArgumentos(argv[1]);
		}

		// Padre
		pid = fork();
		switch(pid) {
			case -1:
				// Error
				perror("fork error");
				printf("errno value= %d\n", errno); 
				exit(EXIT_FAILURE);
			case 0:
				// Ejecuta segundo programa (con argumentos)
				printf("Iniciando %s...\n", argv[2]);
			
				if (execvp(argv[2], &argv[2]) > 0) {
					printf("%s iniciado. PID = %d\n", argv[2], getpid());
				} else {
					perror("exec");
					printf("errno value = %d\n", errno);
					exit(EXIT_FAILURE);
				}
		}

		// Espera terminacion de procesos hijo
		childpid = wait(&status);
		mostrarMsgFinHijo(childpid, status);
		childpid = wait(&status);
		mostrarMsgFinHijo(childpid, status);

		// Fin de programa
		exit(EXIT_SUCCESS);
	} else {
		// Numero de comandos incorrecto
		perror("Formato incorrecto. Introduzca ./programa.exe gnome-calculator gedit fichero1.txt fichero2.txt ficheroN.txt.\n");
		exit(EXIT_FAILURE);
	}
}
