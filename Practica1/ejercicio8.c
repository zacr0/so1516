/*
* @file ejercicio8.c
* @author Pablo Medina Suarez
*
* Programa que expanda un hijo y le envie una señal SIGUSR1 cada segundo.
* El hijo debe imprimir un mensaje cada vez que la reciba mediante la funcion 
* tratarSignal(). Tras 5 mensajes los mensajes deben salir.
*/
#include <sys/types.h>  // estructura pid_t 
#include <errno.h>		// captura de errores
#include <signal.h>		// captura de alarmas
#include <stdio.h>		// entrada/salida
#include <stdlib.h>		// exit()
#include <unistd.h>     // POSIX

static void tratarSignal() {
	puts("Señal recibida.");
}

int main() {
	pid_t pid;
	int num_signals = 0;

	// Inicio de hijo
	pid = fork();
	switch(pid) {
		case -1:
			// Error
			perror("fork error");
			printf("errno value= %d\n", errno); 
			exit(EXIT_FAILURE);
		case 0:
			// Hijo
			printf("Hijo de %d. PID = %d.\n", getppid(), getpid());

			// Cambio comportamiento señal
			if (signal(SIGUSR1, tratarSignal) == SIG_ERR) {
				perror("Error al cambiar comportamiento de señal.");
				exit(EXIT_FAILURE);
			}
			// Modo de espera
			while(1) {
				pause();
			}
		default:
			// Padre
			printf("Padre. PID = %d.\n", getpid());

			while(1) {
				// Envio de señales cada segundo
				sleep(1);
				kill(pid, SIGUSR1);
				num_signals++;

				if (num_signals > 5) {
					// Detiene los procesos
					puts("Deteniendo procesos.");
					kill(pid, SIGKILL);	// finaliza hijo
					exit(EXIT_SUCCESS);
				}
			}
	}

	return 0;
}
