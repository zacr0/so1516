/*
* @file ejercicio7.c
* @author Pablo Medina Suarez
*
* Programa que capture la señal de alarma hasta que tras 4 llamadas se detenga 
* mediante la funcion kill.
*/
#include <signal.h>		// captura de alarmas
#include <stdio.h>		// I/O
#include <unistd.h>     // POSIX

static void alarma() {
	puts("RING!!!!!");
}

int main() {
	int num_alarmas = 0;

	// Cambio de comportamiento señal
	if (signal(SIGALRM, alarma) == SIG_ERR) {
		perror("Error al cambiar comportamiento de señal.");
	}
	// Primera alarma
	puts("Alarma en 5 segundos.");
	alarm(5);
	pause();
	// Segunda alarma
	puts("Alarma en 3 segundos.");
	alarm(3);
	pause();
	// Alarmas cada segundo
	while(1) {
		puts("Alarma en 1 segundo.");
		alarm(1);
		pause();	// espera la señal
		num_alarmas++;
		if (num_alarmas == 4) {
			puts("Alcanzadas 4 alarmas. Deteniendo...");
			raise(SIGKILL);	// envia señal al PID padre
		}
	}

	return 0;
}
