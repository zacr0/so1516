/*
* @file ejercicio1b.c
* @author Pablo Medina Suarez
*
* Creacion de procesos encadenados, siendo cada uno padre del siguiente
*/
#include "io.h"
#include <sys/types.h> 	// estructura pid_t 
#include <sys/wait.h>	// wait()
#include <stdio.h>
#include <stdlib.h>		// exit() y sus macros
#include <unistd.h>     // fork()
#include <errno.h>      // Captura de errores

int main() {
	int num_procesos = 0, status, childpid, i;
	pid_t pid;

	// Lectura de numero de procesos
    do {
        printf("Numero de procesos a crear: ");
        scanf("%d", &num_procesos); 
        getchar();

        if (num_procesos < 1) {
            printf("Introduzca un numero de procesos correcto.\n");
        }
    } while (num_procesos < 1);

    // Creacion de procesos encadenados
    printf("Creando %d procesos...\n", num_procesos);
    for (i = 0; i < num_procesos; i++) {
    	pid = fork();

    	if (pid == 0) {
    		// Proceso hijo
    		printf("Proceso hijo %d; mi padre = %d \n", getpid(), getppid());
    	} else {
    		if (pid) {
	    		// Proceso padre:
	    		childpid = wait(&status);
	    		mostrarMsgFinHijo(childpid, status);
	            exit(EXIT_SUCCESS);
    		} else {
    			// error
    			perror("Fork error");
    			printf("errno = %d\n", errno);
    			exit(EXIT_FAILURE);
    		}
    	}
    }
    exit(EXIT_SUCCESS);
}
