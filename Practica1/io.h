/*
* @file io.h
* @author Pablo Medina Suarez
*
* Declara funciones de alerta por pantalla.
*/
#ifndef IO_H
#define IO_H
	// Mensajes de estado de fin de procesos hijo
	void mostrarMsgFinHijo(int childpid, int status);
	// Comprueba si existe el fichero especificado
	int existeFichero(char *nombreFichero);
#endif
	