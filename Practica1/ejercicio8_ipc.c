/*
* @file ejercicio8_ipc.c
* @author Pablo Medina Suarez
*
* Programa que expanda un hijo y le envie una señal SIGUSR1 cada segundo.
* El hijo debe imprimir un mensaje cada vez que la reciba mediante la funcion 
* tratarSignal() y le envia la señal de escritura de vuelta al proceso padre. 
* Tras 5 mensajes los mensajes deben salir.
*/
#include <sys/types.h>  // estructura pid_t 
#include <errno.h>		// captura de errores
#include <signal.h>		// captura de alarmas
#include <stdio.h>		// entrada/salida
#include <stdlib.h>		// exit()
#include <unistd.h>     // POSIX

static void tratarSignalPadre() {
	puts("Padre. Señal recibida.");
}

static void tratarSignal() {
	puts("Hijo. Señal recibida.");
}

int main() {
	pid_t pid;
	int num_signals = 0;

	// Inicio de hijo
	pid = fork();
	switch(pid) {
		case -1:
			// Error
			perror("fork error");
			printf("errno value= %d\n", errno); 
			exit(EXIT_FAILURE);
		case 0:
			// Hijo
			printf("Hijo de %d. PID = %d.\n", getppid(), getpid());

			// Cambio comportamiento señal
			if (signal(SIGUSR1, tratarSignal) == SIG_ERR) {
				perror("Error al cambiar comportamiento de señal.");
				exit(EXIT_FAILURE);
			}
			// Espera señales del padre
			while(1) {
				pause();
				// Manda señal de recepcion al padre
				kill(getppid(), SIGUSR1);
			}
		default:
			// Padre
			printf("Padre. PID = %d.\n", getpid());

			// Cambio comportamiento de señal
			if (signal(SIGUSR1, tratarSignalPadre) == SIG_ERR) {
				perror("Error al cambiar comportamiento de señal.");
				exit(EXIT_FAILURE);
			}

			while(1) {
				// Envio de señales al hijo cada segundo
				sleep(1);
				kill(pid, SIGUSR1);
				// Espera señal del hijo
				pause();
				num_signals++;

				if (num_signals > 5) {
					// Detiene los procesos
					puts("Deteniendo procesos.");
					kill(pid, SIGKILL);	// finaliza hijo
					exit(EXIT_SUCCESS);
				}
			}
	}

	return 0;
}
