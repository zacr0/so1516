/*
* @file io.c 
* @author Pablo Medina Suarez
*
* Implementacion de las funciones de io.h
*/
#include "io.h"
#include <stdlib.h>
#include <stdio.h>

void mostrarMsgFinHijo(int childpid, int status) {
	if (childpid > 0) {
     	if (WIFEXITED(status)) {
            // Salio con exito
            printf("child %d exited successfully, status=%d\n",childpid, WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            // Finalizado por otro proceso (kill)
            printf("child %d killed (signal %d)\n", childpid, WTERMSIG(status));
        } else if (WIFSTOPPED(status)) {
            // Detenido por el sistema
            printf("child %d stopped (signal %d)\n", childpid, WSTOPSIG(status));
       } 
    } else {
        printf("Error en la invocacion de wait o la llamada ha sido interrumpida por una señal.\n");
        exit(EXIT_FAILURE);
    }
}
int existeFichero(char *nombreFichero) {
    FILE *fich = NULL;
    int existe = 0;

    if ( (fich = fopen(nombreFichero, "r")) != NULL) {
        existe = 1;
        fclose(fich);
    }

    return existe;
}
