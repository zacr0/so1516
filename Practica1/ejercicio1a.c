/*
* @file ejercicio1a.c
* @author Pablo Medina Suarez
*
* Creacion de abanico de procesos.
*/
#include "io.h"
#include <sys/types.h>  // estructura pid_t 
#include <sys/wait.h>   // wait()
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>     // fork()
#include <errno.h>      // captura de errores 

int main() {
    pid_t pid;
    int num_procesos = 0, status, childpid, i;
    
    // Lectura de numero de procesos
    do {
        printf("Numero de procesos a crear: ");
        scanf("%d", &num_procesos); 
        getchar();

        if (num_procesos < 1) {
            printf("Introduzca un numero de procesos correcto.\n");
        }
    } while (num_procesos < 1);

    // Creacion de hijos en abanico
    puts("Creando hijos...");
    for (i = 0; i < num_procesos; i++) {
        pid = fork();
        if (pid == 0) {
            // Proceso hijo
            printf("Proceso hijo %d; mi padre = %d \n", getpid(), getppid());
            exit(EXIT_SUCCESS);
        }
    }

    // Captura de fin de ejecucion de procesos hijo
    for (i = 0; i < num_procesos; i++) {
        childpid = wait(&status);
        // Captura de salida
        mostrarMsgFinHijo(childpid, status);
    }
    exit(EXIT_SUCCESS);
}
