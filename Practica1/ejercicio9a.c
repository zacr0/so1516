/*
* @file ejercicio9a.c
* @author Pablo Medina Suarez
*
* Reciba las señales enviadas desde ejer9b y muestra un mensaje en respuesta
*/
#include <sys/types.h>  // estructura pid_t 
#include <signal.h>		// captura de alarmas
#include <stdio.h>		// entrada/salida
#include <stdlib.h>		// exit()
#include <unistd.h>     // POSIX

static void resSignalUsr() {
	puts("He recibido la señal del otro programa.");
}

int main () {
	// Cambio comportamiento señales
	if (signal(SIGUSR1, resSignalUsr) == SIG_ERR) {
		perror("Error al cambiar comportamiento de señal.");
		exit(EXIT_FAILURE);
	}

	// Modo de espera
	puts("Esperando señales...");
	while(1) {
		pause();
	}
}
