/*
* @file ejercicio2.c
* @author Pablo Medina Suarez
*
* Creacion de procesos zombie
*/
#include <sys/types.h> // estructura pid_t 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>    // fork()
#include <errno.h>     // captura de errores 

int main() {
	pid_t pid;

	pid = fork();
	switch(pid) {
		case -1:
			// Error
			perror("fork error");
            printf("errno value= %d\n", errno);
			exit(EXIT_FAILURE);  
        case 0:
        	// Proceso hijo zombie
            printf("Proceso hijo %d; mi padre = %d \n", getpid(), getppid()); 
            exit(EXIT_SUCCESS);
        default:
        	// Proceso padre que no espera al hijo (puede observarse el proceso zombie con ps -a)
        	sleep(10);
        	exit(EXIT_SUCCESS);
	}
}
