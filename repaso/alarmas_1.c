#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

static void sigHandler() {
	printf("ALLAHUAKBAR\n");
}

int main() {
	pid_t pid;
	int i, childpid, status;

	pid = fork();
	switch(pid) {
		case -1:
			printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
			exit(EXIT_FAILURE);
		case 0:
			// Hijo
			printf("Hijo. PID = %d. PPID = %d.\n", getpid(), getppid());

			// Reemplaza manejador de señal
			if (signal(SIGUSR1, sigHandler) == SIG_ERR) {
				printf("Error signal. Errno = %d. %s.\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}

			// Espera la llegada de una señal
			puts("Hijo esperando señal...");
			while (1) {
				pause();
			}
			//exit(EXIT_SUCCESS);
		default:
			// Padre
			for (i = 0; i < 10; i++) {
				sleep(1);
				puts("Padre enviando señal...");
				kill(pid, SIGUSR1); // envia señal a hijo	
			}
			kill(pid, SIGKILL);

			// Espera al hijo
			childpid = wait(&status);
			if (childpid != -1) {
				if (WIFEXITED(status)) {
					printf("Hijo %d terminado. Status = %d.\n", childpid, WEXITSTATUS(status));
				} else if (WIFSIGNALED(status)) {
					printf("Hijo %d finalizado por señal %d.\n", childpid, WTERMSIG(status));
				} else if(WIFSTOPPED(status)) {
					printf("Hijo %d detenido por %d.\n", childpid, WSTOPSIG(status));
				}
			} else {
				printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
			exit(EXIT_SUCCESS);
	}
}
