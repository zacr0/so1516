#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

void alarmHandler() {
	puts("ALARMA!");
}

int main() {
	if (signal(SIGALRM, alarmHandler) == SIG_ERR) {
		printf("Error signal. Errno = %d. %s.\n", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}

	while (1) {
		alarm(1);
		pause();
	}
	exit(EXIT_SUCCESS);
}
