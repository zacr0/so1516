/*
* Ejecuta dos programas, uno con execlp (ls) indicado en el codigo
* y otro mediante execvp, indicado mediante linea de comandos por
* el usuario.
* Cada uno es ejecutado como un proceso hijo de este programa.
*/
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main(int argc, char **argv) {
	if (argc < 2) {
		perror("Formato incorrecto. Introduzca ./exec_2 comando_a_ejecutar.");
		exit(EXIT_FAILURE);
	}
	pid_t pid;
	int i, childpid, status;

	pid = fork();
	switch(pid) {
		case -1:
			// Error
			printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
			exit(EXIT_FAILURE);
			break;
		case 0: 
			// Hijo (ejecuta ls)
			printf("Hijo 1. PID = %d. PPID = %d.\n", getpid(), getppid());
			if (execlp("ls", "ls", "-l", NULL) == -1) {
				printf("Error execlp. Errno = %d. %s\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
			break;
		default:
			// Padre
			pid = fork();
			switch(pid) {
				case -1:
					// Error
					printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
					break;
				case 0:
					// Hijo (ejecuta comando recibido por linea argumentos)
					printf("Hijo 2. PID = %d. PPID=%d.\n", getpid(), getppid());
					if (execvp(argv[1], &argv[1]) == -1) {
						printf("Error execvp. Errno = %d. %s\n", errno, strerror(errno));
						exit(EXIT_FAILURE);
					}
			}

			// Espera a los hijos
			for (i = 0; i < 2; i++) {
				childpid = wait(&status);
				if (childpid != -1) {
					// Estado de los hijos
					if (WIFEXITED(status)) {
						printf("Hijo %d finalizado correctamente. Status = %d\n", childpid, WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) {
						printf("Hijo %d terminado por señal %d.\n", childpid, WTERMSIG(status));
					}
					if (WIFSTOPPED(status)) {
						printf("Hijo %d detenido por señal %d.\n", childpid, WSTOPSIG(status));
					}
				} else {
					// Error wait
					printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			exit(EXIT_SUCCESS);
			break;
	}
}
