#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
// Variables globales
int num = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* threadFunc(void *itr);

int main(int argc, char **argv) {
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./mutex_1 num_iteraciones.\n");
		exit(EXIT_FAILURE);
	}
	pthread_t th1, th2;
	int error, iteraciones = atoi(argv[1]);

	printf("Van a realizarse %d iteraciones por hilo.\n", iteraciones);
	getchar();

	// Creacion de hilos:
	if ((error = pthread_create(&th1, NULL, threadFunc, (void*) &iteraciones))) {
		switch(error) {
			default:
				// Abreviado
				printf("Error pthread_create");
				exit(EXIT_FAILURE);
				break;
		}
	}
	if ((error = pthread_create(&th2, NULL, threadFunc, (void*) &iteraciones))) {
		switch (error) {
			default:
				// Abreviado
				printf("Error pthread_create");
				exit(EXIT_FAILURE);
				break;
		}
	}

	// Espera a los hilos
	if ((error = pthread_join(th1, NULL))) {
		switch(error) {
			default:
				// Abreviado
				printf("Error pthread_join.\n");
				exit(EXIT_FAILURE);
				break;
		}
	}
	if ((error = pthread_join(th2, NULL))) {
		switch(error) {
			default:
				// Abreviado
				printf("Error pthread_join.\n");
				exit(EXIT_FAILURE);
				break;
		}
	}

	// Resultado
	if ((error = pthread_mutex_lock(&mutex))) {
		printf("Error pthread_mutex_lock.\n");
		exit(EXIT_FAILURE);
	}
	printf("Hilos finalizados. Resultado final = %d.\n", num);
	if ((error = pthread_mutex_unlock(&mutex))) {
		printf("Error pthread_mutex_unlock.\n");
		exit(EXIT_FAILURE);
	}

	// Destruccion de mutex
	if ((error = pthread_mutex_destroy(&mutex))) {
		switch(error) {
			case EBUSY:
				printf("EBUSY. Mutex was locked.");
				exit(EXIT_FAILURE);
				break;
			case EINVAL:
				printf("EINVAL. Invalid value.");
				exit(EXIT_FAILURE);
				break;
			default:
				// Abreviado
				printf("Error en pthread_mutex_destroy.\n");
				exit(EXIT_FAILURE);
				break;
		}
	}
	exit(EXIT_SUCCESS);
}

// Funciones
void* threadFunc(void *itr) {
	int i, error, iteraciones = *((int *) itr);

	for (i = 0; i < iteraciones; i++) {
		// Inicio zona critica
		if ((error = pthread_mutex_lock(&mutex))) {
			printf("Error pthread_mutex_lock.\n");
			exit(EXIT_FAILURE);
		}
		
		num++;
		printf("Thread %lu incrementa la variable. Valor = %d.\n", (unsigned long) pthread_self(), num);

		if ((error = pthread_mutex_unlock(&mutex))) {
			printf("Error pthread_mutex_unlock.\n");
			exit(EXIT_FAILURE);
		}
		// Fin zona critica
		printf("Thread %lu sale de zona critica.\n", (unsigned long) pthread_self());
	}

	pthread_exit(NULL);
}
