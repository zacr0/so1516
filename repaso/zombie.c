#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main() {
	pid_t pid;
	int status, childpid, valor;

	pid = fork();
	if (pid == 0) {
		// Hijo 
		printf("Hijo. PID = %d. PPID = %d.", getpid(), getppid());;
		exit(EXIT_SUCCESS);
	} else if (pid == -1) {
		printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
	}

	sleep(10); // Proceso hijo se queda zombie temporalmente
	childpid = wait(&status);
	if (childpid != -1) {
		// Comprueba terminacion
		if (WIFEXITED(status)) {
			printf("Hijo %d finalizado. Status = %d.\n", childpid, WEXITSTATUS(status));
		} else if(WIFSIGNALED(status)) {
			printf("Hijo %d finalizado (señal %d).\n", childpid, WTERMSIG(status));
		} else if(WIFSTOPPED(status)) {
			printf("Hijo %d detenido (señal %d).\n", childpid, WSTOPSIG(status));
		}
	} else {
		printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}