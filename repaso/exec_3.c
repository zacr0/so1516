/*
* Crea dos procesos, uno calculara el factorial de un numero y otro
* ejecutara el programa indicado por el usuario desde linea de comandos.
*/
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main(int argc, char **argv) {
	if (argc < 3) {
		perror("Formato incorrecto. Introduzca ./exec_2 numero_a_calcular comando_a_ejecutar.");
		exit(EXIT_FAILURE);
	}
	pid_t pid;
	int num = atoi(argv[1]), i, childpid, status, factorial = 1;

	pid = fork();
	switch(pid) {
		case -1:
			// Error
			printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
			exit(EXIT_FAILURE);
			break;
		case 0: 
			// Hijo (ejecuta ls)
			printf("Hijo 1. PID = %d. PPID = %d.\n", getpid(), getppid());
			if (num >= 0) {
				for (i = 1; i <= num; i++) {
					factorial = factorial * i;
				}
				printf("Factorial de %d = %d.\n", num, factorial);
			} else {
				printf("No se puede calcular el factorial de %d.\n", num);
			}
			exit(EXIT_SUCCESS);
			break;
		default:
			// Padre
			pid = fork();
			switch(pid) {
				case -1:
					// Error
					printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
					break;
				case 0:
					// Hijo (ejecuta comando recibido por linea argumentos)
					printf("Hijo 2. PID = %d. PPID=%d.\n", getpid(), getppid());
					if (execvp(argv[2], &argv[2]) == -1) {
						printf("Error execvp. Errno = %d. %s\n", errno, strerror(errno));
						exit(EXIT_FAILURE);
					}
			}

			// Espera a los hijos
			for (i = 0; i < 2; i++) {
				childpid = wait(&status);
				if (childpid != -1) {
					// Estado de los hijos
					if (WIFEXITED(status)) {
						printf("Hijo %d finalizado correctamente. Status = %d\n", childpid, WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) {
						printf("Hijo %d terminado por señal %d.\n", childpid, WTERMSIG(status));
					}
					if (WIFSTOPPED(status)) {
						printf("Hijo %d detenido por señal %d.\n", childpid, WSTOPSIG(status));
					}
				} else {
					// Error wait
					printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			exit(EXIT_SUCCESS);
			break;
	}
}
