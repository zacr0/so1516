#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main() {
	pid_t pid;
	int status, childpid;

	pid = fork();
	switch(pid) {
		case -1:
			printf("Error fork: %d. %s\n", errno, strerror(errno));
			exit(EXIT_FAILURE);
			break;
		case 0:
			// Hijo
			printf("Proceso hijo. PID = %d. PPID = %d.\n", getpid(), getppid());
			// Crea un nieto
			pid = fork();
			switch(pid) {
				case -1:
					// Error
					printf("Error fork: %d. %s\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
					break;
				case 0: 
					// Nieto
					printf("Nieto con PID = %d. Padre = %d.\n", getpid(), getppid());
					exit(EXIT_SUCCESS);
					break;
			}
			// Espera al nieto
			childpid = wait(&status);
			if (childpid != -1) {
				// Status del hijo
				if (WIFEXITED(status)) {
					printf("Nieto PID = %d finalizado. Status = %d\n", childpid, WEXITSTATUS(status));
				}
				// Gestion de errores abreviada
			} else {
				printf("Error wait. Errno = %d. %s\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
			exit(EXIT_SUCCESS);
			break;
		default:
			// Padre
			printf("Proceso padre. PID = %d.\n", getpid());

			// Espera al hijo
			childpid = wait(&status);
			if (childpid != -1) {
				// Comprobacion de estados
				if (WIFEXITED(status)) {
					printf("Hijo terminado correctamente. Status = %d\n", WEXITSTATUS(status));
				} else if (WEXITSTATUS(status)) {
					printf("Hijo terminado por una señal. Status = %d\n", WEXITSTATUS(status));
				}
				// Gestion de error abreviada
			} else {
				// Error en wait
				printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
	}
	exit(EXIT_SUCCESS);
}
