#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

void *funcPrimerHilo(int *param) {
	int num =  *param;
	printf("Hilo 1. Param = %d.\n", num);
	pthread_exit(NULL);
}

void *funcSegundoHilo(char *param) {
	char *cadena = param;
	printf("Hilo 2. Param = %s.\n", cadena);
	pthread_exit(NULL);
}

int main() {
	pthread_t th1, th2;
	int error, val_th1 = getpid();
	char val_th2[] = "soy el hilo 2";

	// Creacion de hilos
	if ((error = pthread_create(&th1, NULL,(void *) funcPrimerHilo, (void *) &val_th1))) {
		// Error
		switch(error) {
			case EAGAIN:
				perror("EAGAIN. Insuficientes recursos.\n");
				break;
			case EINVAL:
				perror("EINVAL. Atributos invalidos.\n");
				break;
			default:
				// Abreviado, habria mas errores
				perror("Error pthread_create.\n");
				exit(EXIT_FAILURE);
		}
	}
	if ((error = pthread_create(&th2, NULL, (void *) funcSegundoHilo, (void *) val_th2))) {
		// Error
		switch(error) {
			case EAGAIN:
				perror("EAGAIN. Insuficientes recursos.\n");
				break;
			case EINVAL:
				perror("EINVAL. Atributos invalidos.\n");
				break;
			default:
				// Abreviado, habria mas errores
				perror("Error pthread_create.\n");
				exit(EXIT_FAILURE);
		}
	}

	// Espera hilos
	if((error = pthread_join(th1, NULL))) {
		switch(error) {
			case EDEADLK:
				perror("EDEADLK. Deadlock detected.\n");
				exit(EXIT_FAILURE);
				break;
			case EINVAL:
				perror("EINVAL. thread not joinable.\n");
				exit(EXIT_FAILURE);
				break;
			default:
				// Abreviado
				perror("Error en pthread_join");
				exit(EXIT_FAILURE);
		}
	}
	puts("Hilo 1 finalizado.");

	if((error = pthread_join(th2, NULL))) {
		switch(error) {
			case EDEADLK:
				perror("EDEADLK. Deadlock detected.\n");
				exit(EXIT_FAILURE);
				break;
			case EINVAL:
				perror("EINVAL. thread not joinable.\n");
				exit(EXIT_FAILURE);
				break;
			default:
				// Abreviado
				perror("Error en pthread_join");
				exit(EXIT_FAILURE);
		}
	}
	puts("Hilo 2 finalizado.");
	exit(EXIT_SUCCESS);
}
