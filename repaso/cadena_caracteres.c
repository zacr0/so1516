#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
	char cadena[] = "buenos dias";
	int i;

	for (i = 0; i < strlen(cadena); i++) {
		printf("%c", cadena[i]);
		fflush(stdout); // impide acumulacion en buffer
		sleep(1);
	}
	exit(EXIT_SUCCESS);
}
