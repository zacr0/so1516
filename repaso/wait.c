#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main() {
	pid_t pid;
	int status, childpid, valor;

	pid = fork();
	if (pid == 0) {
		// Hijo 
		printf("Hijo. PID = %d. PPID = %d.", getpid(), getppid());
		// Asigna un valor de retorno
		//valor = getpid() / 2;
		//exit(valor);
		exit(50);
	}
	childpid = wait(&status);
	if (childpid != -1) {
		// Comprueba terminacion
		if (WIFEXITED(status)) {
			printf("Hijo %d finalizado. Status = %d.\n", childpid, WEXITSTATUS(status));
		} else if(WIFSIGNALED(status)) {
			printf("Hijo %d finalizado (señal %d).\n", childpid, WTERMSIG(status));
		} else if(WIFSTOPPED(status)) {
			printf("Hijo %d detenido (señal %d).\n", childpid, WSTOPSIG(status));
		}
	} else {
		printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}