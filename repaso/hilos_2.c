#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

void *multVocales(char *cad);
void *multNumero(int *num);

int main() {
	pthread_t th_cad, th_num;
	char cadena[] = "Soy una cadena."; // tiene 6 vocales
	int error, num = 295;
	float aux = 0, sum_total = 0;
	void *ret = &aux;
	
	// Creacion de hilos
	if ((error = pthread_create(&th_cad, NULL, (void*)multVocales, (void*) cadena))) {
		switch(error) {
			default: 
				// Abreviado
				perror("Error pthread_create.\n");
				break;
		}
	}
	if ((error = pthread_create(&th_num, NULL, (void*)multNumero, (void*) &num))) {
		switch(error) {
			default: 
				// Abreviado
				perror("Error pthread_create.\n");
				break;
		}
	}

	// Espera finalizacion de los hilos
	if ((error = pthread_join(th_cad, (void **) &ret))) {
		switch(error) {
			case EDEADLK:
				printf("EDEADLK.");
				exit(EXIT_FAILURE);
				break;
			default:
				// Abreviado
				perror("Error pthread_join.");
				exit(EXIT_FAILURE);
				break;
		}
	}
	// Suma temporal en el resultado final
	aux = *((float *) ret);
	sum_total += aux;

	// Espera al segundo hilo
	if ((error = pthread_join(th_num, (void **) &ret))) {
		switch(error) {
			case EDEADLK:
				printf("EDEADLK.");
				exit(EXIT_FAILURE);
				break;
			default:
				// Abreviado
				perror("Error pthread_join.");
				exit(EXIT_FAILURE);
				break;
		}
	}
	aux = *((float *) ret);
	sum_total += aux;

	printf("Resultado final = %.2f.\n", sum_total);
	exit(EXIT_SUCCESS);
}

void *multVocales(char *cad) {
	int i; 
	float *vocales = malloc(sizeof(int));
	*vocales = 0;

	// Lectura de vocales
	for (i = 0; i < strlen(cad); i++) {
		switch (cad[i]) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				(*vocales)++;
				break;
			default:
				break;
		}
	}
	*vocales *= 0.1;
	printf("Vocales en <%s> *0.1 = %.2f.\n", cad, *vocales);
	pthread_exit((void *) vocales);
}

void *multNumero(int *num) {
	float *res = malloc(sizeof(float));
	*res = *num * 0.1;
	printf("Numero %d * 0.1 = %.2f.\n", *num, *res);
	pthread_exit((void *) res);
}
