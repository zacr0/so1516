/*
* Ejecuta dos programas con execlp (ls y gnome-calculator), indicados
* mediante codigo.
* Cada uno es ejecutado como un proceso hijo de este programa.
*/
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main() {
	pid_t pid;
	int i, childpid, status;

	pid = fork();
	switch(pid) {
		case -1:
			// Error
			printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
			exit(EXIT_FAILURE);
			break;
		case 0: 
			// Hijo (ejecuta ls)
			printf("Hijo 1. PID = %d. PPID = %d.\n", getpid(), getppid());
			if (execlp("ls", "ls", "-l", NULL) == -1) {
				printf("Error execlp. Errno = %d. %s\n", errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
			break;
		default:
			// Padre
			pid = fork();
			switch(pid) {
				case -1:
					// Error
					printf("Error fork. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
					break;
				case 0:
					// Hijo (ejecuta gnome-calculator)
					printf("Hijo 2. PID = %d. PPID=%d.\n", getpid(), getppid());
					if (execlp("gnome-calculator", "gnome-calculator", NULL) == -1) {
						printf("Error execlp. Errno = %d. %s\n", errno, strerror(errno));
						exit(EXIT_FAILURE);
					}
			}

			// Espera a los hijos
			for (i = 0; i < 2; i++) {
				childpid = wait(&status);
				if (childpid != -1) {
					// Estado de los hijos
					if (WIFEXITED(status)) {
						printf("Hijo %d finalizado correctamente. Status = %d\n", childpid, WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) {
						printf("Hijo %d terminado por señal %d.\n", childpid, WTERMSIG(status));
					}
					if (WIFSTOPPED(status)) {
						printf("Hijo %d detenido por señal %d.\n", childpid, WSTOPSIG(status));
					}
				} else {
					// Error wait
					printf("Error wait. Errno = %d. %s.\n", errno, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			exit(EXIT_SUCCESS);
			break;
	}
}
