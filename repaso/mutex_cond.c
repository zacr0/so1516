#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
// Globales
int turno = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t le_toca = PTHREAD_COND_INITIALIZER;

void *threadUno();
void *threadDos();

int main(){
	int error;
	pthread_t th1, th2;

	// Creacion de hilos
	puts("Iniciando dos hilos...");
	if ((error = pthread_create(&th1, NULL, threadUno, NULL))) {
		printf("Error pthread_create.\n");
		exit(EXIT_FAILURE);
	}
	if ((error = pthread_create(&th2, NULL, threadDos, NULL))) {
		printf("Error pthread_create.\n");
		exit(EXIT_FAILURE);
	}

	// Espera de hilos
	if ((error = pthread_join(th1, NULL))) {
		printf("Error pthread_join.\n");
		exit(EXIT_FAILURE);
	}
	if ((error = pthread_join(th2, NULL))) {
		printf("Error pthread_join.\n");
		exit(EXIT_FAILURE);
	}

	puts("Los hilos han finalizado.");

	// Finalizacion
	if ((error = pthread_cond_destroy(&le_toca))) {
		printf("Error pthread_cond_destroy.\n");
		exit(EXIT_FAILURE);
	}
	if ((error = pthread_mutex_destroy(&mutex))) {
		printf("Error pthread_mutex_destroy.\n");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}

void *threadUno() {
	int error, turnos = 0;

	while (turnos < 10) {
		// Inicio seccion critica
		if ((error = pthread_mutex_lock(&mutex))) {
			printf("Error pthread_mutex_lock.");
			exit(EXIT_FAILURE);
		}
		if (turno % 2 != 0) {
			puts("Hilo 1 se duerme.");
			if ((error = pthread_cond_wait(&le_toca, &mutex))) {
				printf("Error pthread_cond_wait");
				exit(EXIT_FAILURE);
			}
		}
		// Le toca a este hilo
		turno++;
		turnos++;
		printf("Le toca al hilo 1. Turnos = %d. Total = %d\n", turnos, turno);
		// Fin seccion critica
		if ((error = pthread_mutex_unlock(&mutex))) {
			printf("Error pthread_mutex_unlock.");
			exit(EXIT_FAILURE);
		}

		// Avisa al otro hilo
		if ((error = pthread_cond_signal(&le_toca))) {
			printf("Error pthread_cond_signal.");
			exit(EXIT_FAILURE);
		}
	}

	pthread_exit(NULL);
}

void *threadDos() {
	int error, turnos = 0;

	while (turnos < 10) {
		// Inicio seccion critica
		if ((error = pthread_mutex_lock(&mutex))) {
			printf("Error pthread_mutex_lock.");
			exit(EXIT_FAILURE);
		}
		if (turno % 2 == 0) {
			puts("Hilo 2 se duerme.");
			if ((error = pthread_cond_wait(&le_toca, &mutex))) {
				printf("Error pthread_cond_wait");
				exit(EXIT_FAILURE);
			}
		}
		// Le toca a este hilo
		turno++;
		turnos++;
		printf("Le toca al hilo 2. Turnos = %d. Total = %d. \n", turnos, turno);
		// Fin seccion critica
		if ((error = pthread_mutex_unlock(&mutex))) {
			printf("Error pthread_mutex_unlock.");
			exit(EXIT_FAILURE);
		}

		// Avisa al otro hilo
		if ((error = pthread_cond_signal(&le_toca))) {
			printf("Error pthread_cond_signal.");
			exit(EXIT_FAILURE);
		}
	}

	pthread_exit(NULL);
}