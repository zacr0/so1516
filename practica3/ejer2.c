/*
* @file ejer2.c
* @author Pablo Medina Suarez
*
* Programa a partir del cual se crean 3 hilos diferentes. Cada hilo debe
* escribir un caracter en pantalla cinco veces seguidas, de manera que no haya
* intercalado de carecteres entre los hilos, los cuales deben estar 
* ejecutandose de forma paralela. 
* La hebra principal tambien debe escribir en pantalla.
*/
#include "pthread_error.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// Estructuras
typedef struct params {
	int num_impresiones;
	char caracter;
} params;
// Variables globales
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t permiso_escritura;
int puede_escribir = 1;
// Funciones
void *escribirCaracter(void *param) {
	int i,
		impresiones = ((params *) param)->num_impresiones;
	char caracter = ((params *) param)->caracter;
	
	// Inicio seccion critica
	if (pthread_mutex_lock(&mutex)) {
		lockMutexError();
	}
	while (!puede_escribir) {
		// Espera mientras no pueda escribir en pantalla
		printf("Thread %lu en espera...\n", pthread_self());
		if (pthread_cond_wait(&permiso_escritura, &mutex)) {
			waitCondError();
		}
	}
	puede_escribir = 0;
	if (pthread_mutex_unlock(&mutex)) {
		unlockMutexError();
	}
	
	printf("Thread: %lu. Escribiendo caracter <%c>...\n", pthread_self(), caracter);
	// Escribe caracteres en pantalla cuando pueda
	for (i = 0; i < impresiones; i++) {
		putchar(caracter);
		fflush(stdout);
		//sleep(1);
	}
	
	if (pthread_mutex_lock(&mutex)) {
		lockMutexError();
	}
	puede_escribir = 1;

	if (pthread_cond_signal(&permiso_escritura)) {
		signalCondError();
	}
	if (pthread_mutex_unlock(&mutex)) {
		unlockMutexError();
	}
	// Fin seccion critica

	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// Comprueba numero de parametros
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer2 num_impresiones.\n");
		exit(EXIT_FAILURE);
	}

	int i;
	params parametros[4];
	pthread_t th_mas, th_menos, th_interog;

	if (pthread_cond_init(&permiso_escritura, NULL)) {
		initCondError();
	}

	// Inicializa parametros
	for (i = 0; i < 4; i++) {
		parametros[i].num_impresiones = atoi(argv[1]);
	}

	// Crea hilos
	parametros[0].caracter = '+';
	if (pthread_create(&th_mas, NULL, escribirCaracter, &parametros[0])) {
		createThreadError();
	}
	parametros[1].caracter = '-';
	if (pthread_create(&th_menos, NULL, escribirCaracter, &parametros[1])) {
		createThreadError();
	}
	parametros[2].caracter = '?';
	if (pthread_create(&th_interog, NULL, escribirCaracter, &parametros[2])) {
		createThreadError();
	}

	parametros[3].caracter = '!';
	escribirCaracter(&parametros[3]);


	// Espera hilos
	if (pthread_join(th_mas, NULL)) {
		joinThreadError();
	}
	if (pthread_join(th_menos, NULL)) {
		joinThreadError();
	}
	if (pthread_join(th_interog, NULL)) {
		joinThreadError();
	}

	// Fin
	if(pthread_mutex_destroy(&mutex)) {
		destroyMutexError();
	}
	if (pthread_cond_destroy(&permiso_escritura)) {
		destroyCondError();
	}
	exit(EXIT_SUCCESS);
}
