/*
* @file ejer3a.c
* @author Pablo Medina Suarez
*
* Problema del productor-consumidor (con espera activa):
* Existe un proceso generando valores para un array de enteros con capacidad
* para 5 elementos, al que llamaremos buffer. Hay un consumidor que está 
* extrayendo datos de dicho buffer de uno en uno.
* 
* El sistema esta obligado a impedir la superposicion de las operaciones 
* sobre los datos, solo un agente puede acceder al buffer en un momento
* dado. Por tanto, buffer es la seccion critica.
* 
* Si suponemos que el buffer es limitado y esta completo, el productor debe
* esperar hasta que el consumidor lea al menos un elemento para asi poder
* seguir almacenando datos. En caso de estar vacio, el consumidor debe 
* esperar a que se escriba informacion nueva por parte del productor.
* 
* Haga una implementacion usando mutex pero no variables de condicion.
*/
#include "pthread_error.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
// Constantes
#define TAM_BUFFER 5

// Variables globales
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int buffer[TAM_BUFFER] = {-1,-1,-1,-1,-1};
int elementos_buffer = 0;	// elementos disponibles en buffer

// Funciones
void mostrarVector(int *V, int nEle) {
	int i;
	printf("[");
	for (i = 0; i < nEle; i++) {
		printf("%d", *(V+i));
		if (i < nEle - 1) {
			printf(", ");
		}
	}
	printf("]\n");
}

void *productorThread(void *max) {
	int	max_producciones = *((int *) max), 
		producido, 
		producidos = 0;
	srand(time(NULL) * getpid());

	while (producidos < max_producciones) {
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}
		while ((elementos_buffer < TAM_BUFFER) && (producidos < max_producciones)) {
			/* 
			* Hay posiciones libres en el buffer, se inserta un valor 
			* aleatorio entre 0 y 10 en la ultima posicion libre y se
			* aumenta el contador de elementos producidos y de elementos
			* disponibles en el buffer.
			*/
			producido = rand() % 11;
			buffer[elementos_buffer] = producido;
			elementos_buffer++;
			producidos++;
			printf("Introducido %d en el buffer. Producidos %d/%d elementos...\n", producido, producidos, max_producciones);
		}
		if (elementos_buffer == TAM_BUFFER) {
			// Espera activa (perdida de rendimiento)
			puts("Productor. Buffer lleno, esperando posiciones libres...");
			printf("Vector: ");
			mostrarVector(buffer, TAM_BUFFER);
		}
		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
		sleep(1);
	}
	pthread_exit(NULL);
}

void *consumidorThread(void *max) {
	int max_consumos = *((int *) max),
		consumido,
		consumidos = 0;

	while (consumidos < max_consumos) {
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}
		while (elementos_buffer) {
			/*
			* Mientras haya elementos almacenados en el buffer, son 
			* consumidos reemplazando el valor almacenado en la posicion leida 
			* por -1, decrementando el contador de elementos almacenados y el
			* de elementos leidos.
			*/
			consumido = buffer[elementos_buffer - 1];
			buffer[elementos_buffer - 1] = -1;
			elementos_buffer--;
			consumidos++;
			printf("Extraido %d del buffer.\n", consumido);
		}
		if (elementos_buffer == 0) {
			// Espera activa (perdida de rendimiento)
			printf("Consumidor. Buffer vacio, esperando mas elementos...\n");
			printf("Vector: ");
			mostrarVector(buffer, TAM_BUFFER);
		}
		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
		sleep(1);
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// Comprueba parametros correctos
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer3a max_elementos\n");
		exit(EXIT_FAILURE);
	}

	int max_producciones = atoi(argv[1]);
	pthread_t productor, consumidor;

	printf("Se van a producir %d elementos.\n", max_producciones);
	// Crea hilos
	if (pthread_create(&productor, NULL, productorThread, &max_producciones)) {
		createThreadError();
	}
	if (pthread_create(&consumidor, NULL, consumidorThread, &max_producciones)) {
		createThreadError();
	}

	// Espera hilos
	if (pthread_join(productor, NULL)) {
		joinThreadError();
	}
	if (pthread_join(consumidor, NULL)) {
		joinThreadError();
	}

	// Fin
	printf("FIN. Se han producido %d elementos.\n", max_producciones);
	printf("Vector: ");
	mostrarVector(buffer, TAM_BUFFER);
	if (pthread_mutex_destroy(&mutex)) {
		destroyMutexError();
	}
	exit(EXIT_SUCCESS);
}
