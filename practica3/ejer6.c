/*
* @file ejer6.c
* @author Pablo Medina Suarez
*
* Problema de los filosofos comensales: 
* Algoritmo que permita a los filosofos comer. El algoritmo debe satisfacer
* la exclusion mutua (no puede haber dos filosofos que puedan utilizar el mismo
* tenedor a la vez) y evitar el interbloqueo e inanicion.
*
* Implemente una solucion empleando semaforos. 
*/
#include "semaphore_error.h"
#include "pthread_error.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// Constantes
// Indice del vecino a la izquierda de i
#define IZQ (i - 1 % num_filosofos)
// Indice del vecino a la derecha de i
#define DER (i + 1 % num_filosofos)
#define PENSANDO	0
#define HAMBRIENTO	1
#define COMIENDO	2

// Variables globales
int num_filosofos, *estado_filosofos;
sem_t mutex, *sem_filosofos;

// Funciones
// El filosofo se toma un momento para reflexionar
void pensar(int id) {
	printf("Filosofo %d pensando. Quiza si... Uhm...\n", id);
	sleep(3);
}

void comer(int id) {
	printf("Filosofo %d comiendo...\n", id);
}

// El filosofo comprueba que puede comer, y si puede, cambia su estado
void prueba(int i) {
	if (estado_filosofos[i] == HAMBRIENTO && estado_filosofos[IZQ] != COMIENDO && estado_filosofos[DER] != COMIENDO) {
		estado_filosofos[i] = COMIENDO;

		if (sem_post(&sem_filosofos[i])) {
			postSemError();
		}
	}
}

// El filosofo i intenta comer
void cogerTenedores(int i) {
	// Inicio seccion critica
	if (sem_wait(&mutex)) {
		waitSemError();
	}

	printf("Filosofo %d coge los tenedores.\n", i);
	estado_filosofos[i] = HAMBRIENTO;
	prueba(i);	// intenta coger los tenedores

	if (sem_post(&mutex)) {
		postSemError();
	}
	// Fin seccion critica

	if (sem_wait(&sem_filosofos[i])) {
		waitSemError();
	}
}

// El filosofo suelta los tenedores y comprueba que sus vecinos puedan comer
void dejarTenedores(int i) {
	// Inicio seccion critica
	if (sem_wait(&mutex)) {
		waitSemError();
	}

	printf("Filosofo %d suelta los tenedores.\n", i);
	estado_filosofos[i]	= PENSANDO;
	// comprueba si los vecinos a los lados pueden comer
	prueba(IZQ);
	prueba(DER);

	if (sem_post(&mutex)) {
		postSemError();
	}
	// Fin seccion critica
}


void *filosofo(void *arg) {
	int i = *((int *) arg), espaguetis = 10;

	// Comportamiento filosofo
	while (espaguetis) {
		pensar(i);
		cogerTenedores(i);	// toma dos tenedores
		comer(i);
		dejarTenedores(i);	// suelta ambos tenedores
		espaguetis--;
	}
	printf("Se acabaron los espaguetis para el filosofo %d.\n", i);
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// Comprueba parametros correctos
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer6 num_filosofos.\n");
		exit(EXIT_FAILURE);
	}

	// Obtiene numero de filosofos
	num_filosofos = atoi(argv[1]);
	if (num_filosofos <= 0) {
		perror("Introduzca un valor positivo para num_filosofos.\n");
		exit(EXIT_FAILURE);
	}

	int i, indices[num_filosofos];
	pthread_t th_filosofos[num_filosofos];

	// Reserva de memoria dinamica
	if ((estado_filosofos = malloc(num_filosofos * sizeof(int))) == NULL) {
		perror("Error reserva memoria estado_filosofos.\n");
		exit(EXIT_FAILURE);
	}
	if ((sem_filosofos = malloc(num_filosofos * sizeof(sem_t))) == NULL) {
		perror("Error reserva memoria sem_filosofos.\n");
		exit(EXIT_FAILURE);
	}

	// Inicializacion de semaforos
	if (sem_init(&mutex, 0, 1)) {
		initSemError();
	}
	for (i = 0; i < num_filosofos; i++) {
		if (sem_init(&sem_filosofos[i], 0, 1)) {
			initSemError();
		}
	}

	// Creacion de hilos
	printf("Sentando %d filosofos en la mesa... HORA DE COMER, FIGHT.\n", num_filosofos);
	for (i = 0; i < num_filosofos; i++) {
		indices[i] = i;
		if (pthread_create(&th_filosofos[i], NULL, filosofo, &indices[i])) {
			createThreadError();
		}
	}

	// Recogida de hilos
	for (i = 0; i < num_filosofos; i++) {
		if (pthread_join(th_filosofos[i], NULL)) {
			joinThreadError();
		}
	}

	// Fin
	printf("Almuerzo de %d filosofos finalizado, DEP espaguetis.\n", num_filosofos);
	
	if (sem_destroy(&mutex)) {
		destroySemError();
	}
	for (i = 0; i < num_filosofos; i++) {
		if (sem_destroy(&sem_filosofos[i])) {
			destroySemError();
		}
	}
	free(estado_filosofos);
	free(sem_filosofos);
	exit(EXIT_SUCCESS);
	// No deberias estar viendo esto
	puts("░░░░░░░░▄▀░░░░░░░░░░░░░▒▒▒▒▒▄░░░░░░░░░");
	puts("░░░░░░▄▀▒▒▒▒░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒█░░░░░░░░");
	puts("░░░░░█▒▒▒▒░░░░▒▒▒▒▒▒▒▒▒▒░░░░▒▒█░░░░░░░");
	puts("░░░░█▒▒▄▀▀▀▀▀▄▄▒▒▒▒▒▒▒▒▒▄▄▀▀▀▀▀▀▄░░░░░");
	puts("░░▄▀▒▒▒▄█████▄▒█▒▒▒▒▒▒▒█▒▄█████▄▒█░░░░");
	puts("░█▒▒▒▒▐██▄████▌▒█▒▒▒▒▒█▒▐██▄████▌▒█░░░");
	puts("▀▒▒▒▒▒▒▀█████▀▒▒█▒░▄▒▄█▒▒▀█████▀▒▒▒█░░");
	puts("▒▒▐▒▒▒░░░░▒▒▒▒▒█▒░▒▒▀▒▒█▒▒▒▒░░░░▒▒▒▒█░");
	puts("▒▌▒▒▒░░░▒▒▒▒▒▄▀▒░▒▄█▄█▄▒▀▄▒▒▒░░░░▒▒▒▒▌");
	puts("▒▐▒▒▒▒▒▒▒▒▒▒▒▒▒▌▒▒▀███▀▒▌▒▒▒▒▒▒▒▒▒▒▒▒▌");
	puts("▀▀▄▒▒▒▒▒▒▒▒▒▒▒▌▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▒▒█░");
	puts("▀▄▒▀▄▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▄▄▄▄▒▒▒▒▒▒▄▄▀░░");
	puts("▒▒▀▄▒▀▄▀▀▀▄▀▀▀▀▄▄▄▄▄▄▄▀░░░░▀▀▀▀▀▀░░░░░");
	puts("▒▒▒▒▀▄▐▒▒▒▒▒▒▒▒▒▒▒▒▒▐░░░░░░░░░░░░░░░░░");
	puts("░▄▄▄░░▄░░▄░▄░░▄░░▄░░░░▄▄░▄▄░░░▄▄▄░░░▄▄▄");
	puts("█▄▄▄█░█▄▄█░█▄▄█░░█░░░█░░█░░█░█▄▄▄█░█░░░█");
	puts("█░░░█░░█░░░░█░░░░█░░░█░░█░░█░█░░░█░█░░░█");
	puts("▀░░░▀░░▀░░░░▀░░░░▀▀▀░░░░░░░░░▀░░░▀░▀▄▄▄▀");
}
