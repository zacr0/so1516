/*
* @file ejer4.c
* @author Pablo Medina Suarez
*
* Problema master-broker:
* Programa que a partir de un fichero de texto, realice concurrentemente tantas
* copias del mismo como se indique. Recibira por la linea de comandos dos 
* parametros (nombre del fichero y numero de copias a realizar).
* El nombre del fichero de salida sera el mismo que el del original, seguido
* por un numero de copia.
* 
* Habra una hebra se encargara de leer el fichero original, y otras tantas
* como copias haya que hacer. Ira cargando caracteres en un buffer de tamaño 
* fijo. El resto de hilos iran copiando bloques de memoria en su copia de 
* fichero.
* Implemente la solucion mediante mutex y variables de condicion.
*/
#include "pthread_error.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// Constantes
#define TAM_BUFFER 64

// Estructuras
typedef struct params_escritor {
	char *ruta_fichero;
	int id;
} params_escritor;

// Variables globales
char buffer[TAM_BUFFER];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t puede_leer = PTHREAD_COND_INITIALIZER;
pthread_cond_t puede_escribir = PTHREAD_COND_INITIALIZER;
int *escritores_finalizados, num_escritores, fin_lectura = 0;

// Funciones
int hanFinalizadoEscritores() {
	int i;
	for (i = 0; i < num_escritores; i++) {
		if (!escritores_finalizados[i]) {
			return 0;
		}
	}
	return 1;
}

void *lectorThread(void *ruta_fichero) {
	int i;
	char *ruta = (char *) ruta_fichero;
	FILE *fich_origen;
	printf("Leyendo fichero \"%s\"\n", ruta);
	
	// Apertura fichero origen
	if ((fich_origen = fopen(ruta, "r")) == NULL) {
		fprintf(stderr, "Error al abrir \"%s\"\n", ruta);
		exit(EXIT_FAILURE);
	}

	while (!fin_lectura) {
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}

		// Lectura fichero origen
		fgets(buffer, sizeof(buffer), fich_origen);
		printf("Lector. Leido \"%s\".\n", buffer);
		for (i = 0; i < num_escritores; i++) {
			escritores_finalizados[i] = 0;
		}
		// Avisa a los escritores que hay nuevo trabajo contenido
		if (pthread_cond_broadcast(&puede_escribir)) {
			signalCondError();
		}

		// Comprueba fin de lectura del fichero
		if (feof(fich_origen)) {
			puts("Lector. Fin de fichero de origen alcanzado.");
			fin_lectura = 1;
		}

		// Espera que los escritores acaben de leer el buffer actual
		puts("Lector. Esperando a escritores...");
		if (pthread_cond_wait(&puede_leer, &mutex)) {
			waitCondError();
		}
		//if (fin_lectura) break;
		
		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
		// sleep(5);
	}

	// Fin de lectura
	fclose(fich_origen);
	pthread_exit(NULL);
}

void *escritorThread(void *params) {
	params_escritor parametros = *((params_escritor *) params);
	int id = parametros.id;
	char ruta_copia[256], *ruta_origen = parametros.ruta_fichero;
	sprintf(ruta_copia, "%s_%d.txt", ruta_origen, id);
	FILE *fich_copia;
	printf("Escritor %d. Escribiendo fichero \"%s\"\n", id, ruta_copia);
	
	// Apertura fichero copia
	if ((fich_copia = fopen(ruta_copia, "w")) == NULL) {
		fprintf(stderr, "Error al abrir \"%s\"\n", ruta_copia);
		exit(EXIT_FAILURE);
	}
	
	while (!fin_lectura) {
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}
		if (escritores_finalizados[id]) {
			// Espera llenado de buffer
			printf("Escritor %d. Esperando llenado de buffer...\n", id);
			if (pthread_cond_wait(&puede_escribir, &mutex)) {
				waitCondError();
			}
		}

		// Escritura de fichero copia
		fputs(buffer, fich_copia);
		escritores_finalizados[id] = 1;
		printf("Escritor %d. Escrito \"%s\".\n", id, buffer);
		
		if (hanFinalizadoEscritores()) {
			// Notifica al lector que todos los escritores han acabado su 
			// trabajo y puede leer el siguiente bloque.
			if (pthread_cond_signal(&puede_leer)) {
				signalCondError();
			}
			//if (fin_lectura) break;
		}

		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
		//sleep(5);
	}
	// Fin de lectura
	fclose(fich_copia);
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	if (argc != 3) {
		// Comprueba parametros correctos
		perror("Formato incorrecto. Introduzca ./ejer4 ruta_fichero num_copias\n");
		exit(EXIT_FAILURE);
	}
	char *ruta_fichero = argv[1];
	int i, num_copias = atoi(argv[2]);
	pthread_t th_lector, th_escritores[num_copias];
	params_escritor params_escritores[num_copias];
	num_escritores = num_copias;

	if (num_copias <= 0) {
		perror("Introduzca un valor positivo para realizar copias.\n");
		exit(EXIT_FAILURE);
	}
	
	// Reserva e inicializacion de vector de escritores listos para leer
	if ((escritores_finalizados = calloc(num_copias, sizeof(int))) == NULL) {
		perror("Fallo al reservar memoria.\n");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < num_copias; i++) {
		escritores_finalizados[i] = 1;
	}

	// Creacion de hilos
	puts("Iniciando lector...");
	if (pthread_create(&th_lector, NULL, lectorThread, ruta_fichero)) {
		createThreadError();
	}
	printf("Iniciando %d escritores...\n", num_copias);
	for (i = 0; i < num_copias; i++) {
		// Creacion de escritores
		params_escritores[i].ruta_fichero = ruta_fichero;
		params_escritores[i].id = i;
		if (pthread_create(&th_escritores[i], NULL, escritorThread, &params_escritores[i])) {
			createThreadError();
		}
	}

	// Espera hilos
	if (pthread_join(th_lector, NULL)) {
		joinThreadError();
	}
	for (i = 0; i < num_copias; i++) {
		if (pthread_join(th_escritores[i], NULL)) {
			joinThreadError();
		}
	}

	// Fin
	printf("Finalizacion de hilos, se han creado %d copias de \"%s\".\n", num_copias, ruta_fichero);
	if (pthread_cond_destroy(&puede_leer)) {
		destroyCondError();
	}
	if (pthread_cond_destroy(&puede_escribir)) {
		destroyCondError();
	}
	if (pthread_mutex_destroy(&mutex)) {
		destroyMutexError();
	}
	free(escritores_finalizados);
	exit(EXIT_SUCCESS);
}
