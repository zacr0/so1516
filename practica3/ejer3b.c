/*
* @file ejer3b.c
* @author Pablo Medina Suarez
*
* Problema del productor-consumidor (sin espera activa):
* Cuando el consumidor toma un producto notifica al productor que puede 
* comenzar a trabajar nuevamente porque al menos hay un hueco libre. Si el 
* buffer se vacia el consumidor se pone a dormir y en el momento en que el
* productor agrega un producto crea una señal para despertarlo.
*
* Utilice variables de condicion junto con mutex para evitar espera activa.
*/
#include "pthread_error.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
// Constantes
#define TAM_BUFFER 5

// Variables globales
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t buffer_lleno = PTHREAD_COND_INITIALIZER,
				buffer_vacio = PTHREAD_COND_INITIALIZER;
int buffer[TAM_BUFFER] = {-1,-1,-1,-1,-1};
int elementos_buffer = 0,
	puede_consumir = 0, 
	puede_producir = 1;

// Funciones
void mostrarVector(int *V, int nEle) {
	int i;
	printf("[");
	for (i = 0; i < nEle; i++) {
		printf("%d", *(V+i));
		if (i < nEle - 1) {
			printf(", ");
		}
	}
	printf("]\n");
}

void *productorThread(void *max_productos) {
	int max_producciones = *((int *) max_productos),
		pos_productor = 0,
		producido,
		producidos = 0;
	srand(time(NULL) * getpid());

	while (producidos < max_producciones) {
		//sleep(2);
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}
		if (!puede_producir) {
			// Espera vaciado de buffer
			puts("Productor. Esperando vaciado de buffer...");
			mostrarVector(buffer, TAM_BUFFER);
			if (pthread_cond_wait(&buffer_vacio, &mutex)) {
				waitCondError();
			}
		}

		// Produce valores [0-10] y los introduce en el buffer si hay espacio
		producido = rand() % 11;
		buffer[pos_productor] = producido;
		printf("Productor. Introducido %d en buffer[%d]...", producido, pos_productor);
		
		// Incremento de contadores de desplazamiento y control
		elementos_buffer++;
		producidos++;
		pos_productor++;
		pos_productor %= TAM_BUFFER;
		printf(" Progreso: %d/%d\n", producidos, max_producciones);
		
		// Modificacion variables de condicion
		puede_consumir = 1;
		if (elementos_buffer == TAM_BUFFER) {
			// Buffer lleno
			puede_producir = 0;
		}

		if (pthread_cond_signal(&buffer_lleno)) {
			signalCondError();
		}
		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
	}
	pthread_exit(NULL);
}

void *consumidorThread(void *max_productos) {
	int max_consumos = *((int *) max_productos),
		pos_consumidor = 0,
		consumido, 
		consumidos = 0;

	while (consumidos < max_consumos) {
		//sleep(2);
		// Inicio seccion critica
		if (pthread_mutex_lock(&mutex)) {
			lockMutexError();
		}
		if (!puede_consumir) {
			// Espera llenado de buffer
			puts("Consumidor. Esperando llenado de buffer...");
			mostrarVector(buffer, TAM_BUFFER);
			if (pthread_cond_wait(&buffer_lleno, &mutex)) {
				waitCondError();
			}
		}
		// Consume valores almacenados en el buffer (si hay)
		consumido = buffer[pos_consumidor];
		buffer[pos_consumidor] = -1;

		// Incremento de contadores de desplazamiento y control
		consumidos++;
		printf("Consumidor. Extraido %d en buffer[%d]... Progreso: %d/%d\n", consumido, pos_consumidor, consumidos, max_consumos);
		pos_consumidor++;
		pos_consumidor %= TAM_BUFFER;
		elementos_buffer--;

		// Modificacion variables de condicion
		puede_producir = 1;
		if (!elementos_buffer) {
			// Buffer vacio
			puede_consumir = 0;
		}

		if (pthread_cond_signal(&buffer_vacio)) {
			signalCondError();
		}
		if (pthread_mutex_unlock(&mutex)) {
			unlockMutexError();
		}
		// Fin seccion critica
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// Comprueba parametros correctos
	if (argc != 2) {
		perror("Formato incorrecto. Introduzca ./ejer3b max_elementos\n");
		exit(EXIT_FAILURE);
	}

	int max_producciones = atoi(argv[1]);
	pthread_t th_productor, th_consumidor;

	printf("Buffer inicial: ");
	mostrarVector(buffer, TAM_BUFFER);

	printf("Se van a producir %d elementos.\n", max_producciones);
	// Crea hilos
	if (pthread_create(&th_productor, NULL, productorThread, &max_producciones)) {
		createThreadError();
	}
	if (pthread_create(&th_consumidor, NULL, consumidorThread, &max_producciones)) {
		createThreadError();
	}

	// Espera hilos
	if (pthread_join(th_productor, NULL)) {
		joinThreadError();
	}
	if (pthread_join(th_consumidor, NULL)) {
		joinThreadError();
	}

	// Fin
	printf("FIN. Se han producido %d elementos.\n", max_producciones);
	if (pthread_cond_destroy(&buffer_lleno)) {
		destroyCondError();
	}
	if (pthread_mutex_destroy(&mutex)) {
		destroyMutexError();
	}
	exit(EXIT_SUCCESS);
}
