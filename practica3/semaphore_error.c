/*
* @file semaphore_error.c
* @author Pablo Medina Suarez
*
* Implementacion de las funciones declaradas en semaphore_error.h.
*/
#include "semaphore_error.h"
#include <errno.h>
#include <stdio.h>

void initSemError() {
	switch(errno) {
		case EINVAL:
			perror("The value argument exceeds {SEM_VALUE_MAX}.\n");
			break;
		case ENOSPC:
			perror("A resource required to initialize the semaphore has been exhausted, or the limit on semaphores ( {SEM_NSEMS_MAX}) has been reached.\n");
				break;
		case EPERM:
			perror("The process lacks appropriate privileges to initialize the semaphore.\n");
			break;
		default:
			perror("Error sem_init.\n");
			break;
	}
}

void waitSemError() {
	switch(errno) {
		case EDEADLK:
			perror("A deadlock condition was detected.\n");
			break;
		case EINTR:
			perror("A signal interrupted this function.\n");
			break;
		case EINVAL:
			perror("The sem argument does not refer to a valid semaphore.\n");
			break;
		default:
			perror("Error sem_wait.\n");
			break;
	}
}

void postSemError() {
	switch(errno) {
		case EINVAL:
			perror("The sem argument does not refer to a valid semaphore.\n");
			break;
		default:
			perror("Error sem_post\n");
			break;
	}
}

void getValueSemError() {
	switch(errno) {
		case EINVAL:
			perror("The sem argument does not refer to a valid semaphore.\n");
			break;
		default:
			perror("Error sem_getvalue.\n");
			break;
	}
}

void destroySemError() {
	switch(errno) {
		case EINVAL:
			perror("The sem argument is not a valid semaphore.\n");
			break;
		case EBUSY:
			perror("There are currently processes blocked on the semaphore.\n");
			break;
		default:
			perror("Error sem_destroy.\n");
			break;
	}
}
