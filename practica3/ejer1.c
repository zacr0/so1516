/*
* @file ejer1.c
* @author Pablo Medina Suarez
*
* Programa que genera N clientes y M proveedores (misma cantidad de 
* proveedores que modelos de camiseta). Se realizan las operaciones:
* 	- Compras: cada hilo cliente debe generar un valor aleatorio para el
*	modelo de camiseta y otro para la cantidad a comprar. Debe decrementar
*   el stock de la camiseta en cuestion.
*   - Suministro: cada hilo proveedor debe hacer lo mismo que el cliente 
*   en este caso incrementando el stock de la camiseta.
*/
#include "pthread_error.h"	// gestion de errores
#include <pthread.h> 		// threads
#include <stdlib.h> 		// I/O exit
#include <stdio.h>			// I/O
#include <time.h>			// rand()
#include <unistd.h>			// getpid()
// Constantes
#define CLIENTES 7			// clientes
#define PROVEEDORES 5		// proveedores / modelos

// Variables globales
int camisetas[PROVEEDORES];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// Funciones
void *clientThread() {
	int modelo, cantidad;
	// Cliente compra 0-10 unidades de un modelo concreto
	modelo = rand() % PROVEEDORES;
	cantidad = rand() % 11;

	// Inicio seccion critica
	if (pthread_mutex_lock(&mutex)) {
		lockMutexError();
	}
	// Decrementa el stock de un modelo de camiseta 
	camisetas[modelo] -= cantidad;
	if (camisetas[modelo] < 0) {
		camisetas[modelo] = 0;
	}

	if (pthread_mutex_unlock(&mutex)) {
		unlockMutexError();
	}
	// Fin seccion critica

	printf("CLIENTE: %lu. Compradas %d unidades del modelo %d.\n", pthread_self(), cantidad, modelo);
	pthread_exit(NULL);
}

void *providerThread() {
	int modelo, cantidad;
	// Proveedor suministra 0-10 unidades de un modelo concreto
	modelo = rand() % PROVEEDORES;
	cantidad = rand() % 11;

	// Inicio seccion critica
	if (pthread_mutex_lock(&mutex)) {
		lockMutexError();
	}
	
	camisetas[modelo] += cantidad;

	if (pthread_mutex_unlock(&mutex)) {
		unlockMutexError();
	}
	// Fin seccion critica

	printf("PROVEEDOR: %lu. Suministradas %d camisetas del modelo %d.\n", pthread_self(), cantidad, modelo);
	pthread_exit(NULL);
}

void mostrarVector(int *V, int nEle) {
	int i;
	printf("[");
	for (i = 0; i < nEle; i++) {
		printf("%d", *(V+i));
		if (i < nEle - 1) {
			printf(", ");
		}
	}
	printf("]\n");
}

int main(int argc, char **argv) {
	int i;
	pthread_t thCli[CLIENTES], thProv[PROVEEDORES];
	srand(time(NULL) * getpid());

	// Inicializacion de stock de camisetas inicial (valores 0 - 30)
	for (i = 0; i < PROVEEDORES; i++) {
		camisetas[i] = rand() % 31;
	}
	puts("Thread principal. Stock de camisetas inicial:");
	mostrarVector(camisetas, PROVEEDORES);

	// Crea hilos
	printf("Creando %d clientes...\n", CLIENTES);
	for (i = 0; i < CLIENTES; i++) {
		if (pthread_create(&thCli[i], NULL, clientThread, camisetas)) {
			createThreadError();
		}
	}
	printf("Creando %d proveedores...\n", PROVEEDORES);
	for (i = 0; i < PROVEEDORES; i++) {
		if (pthread_create(&thProv[i], NULL, providerThread, camisetas)) {
			createThreadError();
		}
	}

	// Espera finalizacion hilos
	for (i = 0; i < CLIENTES; i++) {
		if (pthread_join(thCli[i], NULL)) {
			joinThreadError();
		}
	}
	for (i = 0; i < PROVEEDORES; i++) {
		if (pthread_join(thProv[i], NULL)) {
			joinThreadError();
		}
	}

	// resultados
	puts("Thread principal. Stock de camisetas final:");
	mostrarVector(camisetas, PROVEEDORES);

	if (pthread_mutex_destroy(&mutex)) {
		destroyMutexError();
	}
	exit(EXIT_SUCCESS);
}
