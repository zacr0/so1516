/*
* @file pthread_error.h
* @author Pablo Medina Suarez
*
* Declaracion de funciones para manejo de errores de las funciones
* de la libreria phtread.
*/
#ifndef PTHREAD_ERROR_H
#define PTHREAD_ERROR_H
// Threads
void createThreadError();
void joinThreadError();
// Mutex
void initMutexError();
void lockMutexError();
void unlockMutexError();
void destroyMutexError();
// Variables de condicion
void initCondError();
void waitCondError();
void signalCondError();
void broadcastCondError();
void destroyCondError();
#endif
