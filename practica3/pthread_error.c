/*
* pthread_error.c
* Implementacion de las funciones declaradas en pthread_error.h
*/
#include "pthread_error.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

// Threads
void createThreadError() {
	switch(errno) {
		case EAGAIN:
			perror("System lacked resources to create another thread, or PTHREAD_THREADS_MAX exceeded\n");
			break;
		case EINVAL:
			perror("Invalid attr value.\n");
			break;
		case EPERM:
			perror("Insufficient permissions to set scheduling permissions.\n");
			break;
		default:
			perror("Error pthread_create.\n");
			break;
	}
	exit(EXIT_FAILURE);
}

void joinThreadError() {
	switch(errno) {
		case EINVAL:
			perror("Thread specified not joinable.\n");
			break; 
		case ESRCH:
			perror("Thread ID not found\n");
			break; 
		case EDEADLK:
			perror("Deadlock detected, or thread specifies the calling thread\n");
			break; 
		default:
			perror("Error pthread_join\n");
			break;
	}
	exit(EXIT_FAILURE);
}

// Mutex
void initMutexError() {
	switch(errno) {
		case EAGAIN:
			perror("System lacked the necessary resources to initialize another mutex.\n");
			break;
		case ENOMEM:
			perror("Insufficent memory exist to initilize the mutex.\n");
			break;
		case EPERM:
			perror("Caller does not have the privilege to perform the operation.\n");
			break;
		case EINVAL:
			perror("The atributes object referenced by attr has the robust mutex attribute set without the process-shared attribute being set.\n");
			break;
		default:
			perror("Error pthread_mutex_init.\n");
			break;
	}
}

void lockMutexError() {
	switch(errno) {
		case EAGAIN:
			perror("The mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded.\n");
			break;
		case EINVAL:
			perror("The mutex was created with the protocol attribute having the value PTHREAD_PRIO_PROTECT and the calling threads priority is higher than the mutex's current priority ceiling.\n");
			break;
		case ENOTRECOVERABLE:
			perror("The state protected by the mutex is not recoverable.\n");
			break;
		case EOWNERDEAD:
			perror("The mutex is a robust mutex and the process containing the previous owning thread terminated while holding the mutex lock. The mutex lock shall be acquired by the calling thread and it is up to the new owner to make the state consistent.\n");
			break;
		case EDEADLK:
			perror("The mutex type is PTHREAD_MUTEX_ERRORCHECK and the current thread already owns the mutex or a deadlock condition was detected.\n");
			break;
		default:
			perror("Error pthread_mutex_lock.\n");
			break;
	}
}

void unlockMutexError() {
	switch(errno) {
		case EPERM:
			perror("The mutex type is PTHREAD_MUTEX_ERRORCHECK or PTHREAD_MUTEX_RECURSIVE, or the mutex is a robust mutex, and the current thread does not own the mutex.\n");
			break;
		default:
			perror("Error pthread_mutex_unlock.\n");
			break;
	}
}

void destroyMutexError() {
	switch(errno) {
		case EBUSY:
			perror("The implementation has detected an attempt to destroy the object referenced by mutex while it is locked or referenced.\n");
			break;
		case EINVAL:
			perror("The value specified by mutex is invalid.\n");
			break;
		default:
			perror("Error pthread_mutex_destroy.\n");
			break;
	}
}

// Variables de condicion
void initCondError() {
	switch(errno) {
		case EAGAIN:
			perror("The system lacked the necessary resources (other than memory) to initialise another condition variable.\n");
			break;
		case ENOMEM:
			perror("Insufficient memory exists to initialise the condition variable.\n");
			break;
		case EBUSY:
			perror("The implementation has detected an attempt to re-initialise the object referenced by cond, a previously initialised, but not yet destroyed, condition variable.\n");
			break;
		case EINVAL:
			perror("The value specified by attr is invalid.\n");
			break;
		default:
			perror("Error pthread_cond_init.\n");
			break;
	}
}

void waitCondError() {
	switch(errno) {
		case EINVAL:
			perror("The value specified by cond, mutex, or abstime is invalid or different mutexes were supplied for concurrent pthread_cond_wait() operations on the same condition variable or the mutex was not owned by the current thread at the time of the call.\n");
			break;
		default:
			perror("Error pthread_cond_wait.\n");
			break;
	}
}

void signalCondError() {
	switch(errno) {
		case EINVAL:
			perror("The value cond does not refer to an initialised condition variable.\n");
			break;
		default:
			perror("Error pthread_cond_signal.\n");
			break;
	}
}

void broadcastCondError() {
	switch(errno) {
		case EINVAL:
			perror("The value cond does not refer to an initialized condition variable.\n");
			break;
		default:
			perror("Error pthread_cond_broadcast.\n");
			break;
	}
}

void destroyCondError() {
	switch(errno) {
		case EBUSY:
			perror("The implementation has detected an attempt to destroy the object referenced by cond while it is referenced.\n");
			break;
		case EINVAL:
			perror("The value specified by cond is invalid.\n");
			break;
		default:
			perror("Error pthread_cond_destroy.\n");
			break;
	}
}
