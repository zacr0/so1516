/*
* @file ejer5.c
* @author Pablo Medina Suarez
* 
* Solucion del problema del productor-consumidor utilizando un buffer circular 
* y acotado usando semaforos generales. 
* Haga su programa generico para que se pueda indicar el tamaño del buffer y 
* la cantidad de datos a producir-consumir.
*/
#include "semaphore_error.h"
#include "pthread_error.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
// Variables globales
int *buffer;
sem_t mutex, 			 // control acceso a regiones criticas
	posiciones_libres,   // control escritura y lectura en buffer
	posiciones_ocupadas; // control escritura y lectura en buffer

// Estructuras
typedef struct params {
	int tam_buffer, num_productos;
} params;

// Funciones
void mostrarVector(int *V, int nEle) {
	int i;
	printf("[");
	for (i = 0; i < nEle; i++) {
		printf("%d", *(V+i));
		if (i < nEle - 1) {
			printf(", ");
		}
	}
	printf("]\n");
}

void *productorThread(void *args) {
	int producido, 
		pos_productor = 0, // indice donde insertar el siguiente elemento
		producidos = 0, 
		max_producciones =((params *) args)->num_productos, 
		tam_buffer = ((params *) args)->tam_buffer;
	srand(time(NULL) * getpid());

	while (producidos < max_producciones) {
		// Espera que haya posiciones libres en buffer
		if (sem_wait(&posiciones_libres)) {
			waitSemError();
		}
		// Inicio seccion critica
		if (sem_wait(&mutex)) {
			waitSemError();
		}

		producido = rand() % 11;
		buffer[pos_productor] = producido;
		producidos++;
		printf("Introducido %d en buffer[%d]. Producidos %d/%d elementos...\n", producido, pos_productor, producidos, max_producciones);
		pos_productor++;
		pos_productor %= tam_buffer;
		mostrarVector(buffer, tam_buffer);

		if (sem_post(&mutex)) {
			waitSemError();
		}
		if (sem_post(&posiciones_ocupadas)) {
			waitSemError();
		}
		// Fin seccion critica
	}
	pthread_exit(NULL);
}

void *consumidorThread(void *args) {
	int consumido,
		pos_consumidor = 0,
		consumidos = 0, 
		max_consumos = ((params *) args)->num_productos,
		tam_buffer = ((params *) args)->tam_buffer;

	while (consumidos < max_consumos) {
		if (sem_wait(&posiciones_ocupadas)) {
			waitSemError();
		}
		// Inicio seccion critica
		if (sem_wait(&mutex)) {
			waitSemError();
		}

		consumido = buffer[pos_consumidor];
		buffer[pos_consumidor] = -1;
		consumidos++;
		printf("Extraido %d de buffer[%d]. Extraidos %d/%d elementos...\n", consumido, pos_consumidor, consumidos, max_consumos);
		pos_consumidor++;
		pos_consumidor %= tam_buffer;
		mostrarVector(buffer, tam_buffer);

		if (sem_post(&mutex)) {
			postSemError();
		}
		if (sem_post(&posiciones_libres)) {
			postSemError();
		}
		// Fin seccion critica
	}

	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// Comprueba parametros correctos
	if (argc != 3) {
		perror("Formato incorrecto. Introduzca ./ejer5 tamano_buffer cantidad_de_datos.\n");
		exit(EXIT_FAILURE);
	}
	int tam_buffer = 0, num_productos = 0;
	pthread_t th_productor, th_consumidor;
	params parametros;
	tam_buffer = atoi(argv[1]);
	num_productos = atoi(argv[2]);

	// Comprueba parametros validos
	if (tam_buffer <= 0) {
		perror("Formato incorrecto. Introduzca un valor positivo para tamano_buffer.\n");
		exit(EXIT_FAILURE);
	}
	if (num_productos <= 0) {
		perror("Formato incorrecto. Introduzca un valor positivo para cantidad_de_datos.\n");
		exit(EXIT_FAILURE);
	}

	// Inicializa buffer dinamico de tamano indicado
	if ((buffer = malloc(tam_buffer * sizeof(int))) == NULL) {
		perror("Error al reservar memoria.\n");
		exit(EXIT_FAILURE);
	}

	// Inicializa semaforo:
	if (sem_init(&mutex, 0, 1)){
		initSemError();
	}
	if (sem_init(&posiciones_libres, 0, tam_buffer)) {
		initSemError();
	}
	if (sem_init(&posiciones_ocupadas, 0, 0)) {
		initSemError();
	}

	// Creacion de hilos
	parametros.tam_buffer = tam_buffer;
	parametros.num_productos = num_productos;
	puts("Iniciando productor...");
	if (pthread_create(&th_productor, NULL, productorThread, &parametros)) {
		createThreadError();
	}
	puts("Iniciando consumidor...");
	if (pthread_create(&th_consumidor, NULL, consumidorThread, &parametros)) {
		createThreadError();
	}

	// Recogida de hilos
	if (pthread_join(th_productor, NULL)) {
		joinThreadError();
	}
	if (pthread_join(th_consumidor, NULL)) {
		joinThreadError();
	}

	// Fin
	printf("Finalizacion de los hilos. Se han producido %d elementos.\n", num_productos);
	if (sem_destroy(&mutex)) {
		destroySemError();
	}
	if (sem_destroy(&posiciones_libres)) {
		destroySemError();
	}
	if (sem_destroy(&posiciones_libres)) {
		destroySemError();
	}
	free(buffer);
	exit(EXIT_SUCCESS);
}
