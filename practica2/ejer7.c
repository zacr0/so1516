/*
* @file ejer7.c
* @author Pablo Medina Suarez
*
* Cree dos hebras y que cada una incremente 50 veces en un bucle una variable 
* global (cuyo espacio de memoria es compartido).
* Imprima al final del programa principal el valor de la variable (cuyo valor
* probablemente sea incorrecto). Intente razonar el resultado.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <pthread.h>		// Manejo de hilos

// VARIABLES GLOBALES
int valor_global = 0;

// FUNCIONES
void *incValorGlobal(void *num_iteraciones);

int main() {
	int i, num_hilos = 2, num_iteraciones = 0;
	pthread_t threads[num_hilos];

	// Lectura cantidad de iteraciones
	do {
		printf("Cantidad de incrementos: ");
		scanf("%d", &num_iteraciones);
		getchar();

		if (num_iteraciones <= 0) {
			puts("Error. Introduzca un numero positivo.");
		}
	} while(num_iteraciones <= 0);

	// Creacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_create(&(threads[i]), NULL, incValorGlobal, &num_iteraciones)) {
			createThreadError();
		}
	}

	// Espera finalizacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], NULL)) {
			joinThreadError();
		}
	}

	/*
	* Muestra resultado de la variable global, puede apreciarse como con un 
	* numero alto de iteraciones aparecen los errores de concurrencia. Es
	* decir, el valor de la variable global obtenido es superior al esperado.
	*/
	printf("Thread principal. Valor global = %d.\n", valor_global);
	exit(EXIT_SUCCESS);
}

void *incValorGlobal(void *num_iteraciones) {
	int i, iteraciones = *((int *) num_iteraciones);

	for (i = 0; i < iteraciones; i++) {
		valor_global++;
	}
	pthread_exit(NULL);
}
