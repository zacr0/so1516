/*
* @file pthread_error.c
* @author Pablo Medina Suarez
*
* Implementacion de las funciones declaradas en pthread_error.h
*/
#include "pthread_error.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

// Threads
void createThreadError() {
	switch(errno) {
		case EAGAIN:
			perror("System lacked resources to create another thread, or PTHREAD_THREADS_MAX exceeded");
			break;
		case EINVAL:
			perror("Invalid attr value.");
			break;
		case EPERM:
			perror("Insufficient permissions to set scheduling permissions.");
			break;
		default:
			perror("Error pthread_create.");
			break;
	}
	exit(EXIT_FAILURE);
}

void joinThreadError() {
	switch(errno) {
		case EINVAL:
			perror("Thread specified not joinable.");
			break; 
		case ESRCH:
			perror("Thread ID not found");
			break; 
		case EDEADLK:
			perror("Deadlock detected, or thread specifies the calling thread");
			break; 
		default:
			perror("Error pthread_join");
			break;
	}
	exit(EXIT_FAILURE);
}

// Mutex
void initMutexError() {
	switch(errno) {
		case EAGAIN:
			perror("System lacked the necessary resources to initialize another mutex.");
			break;
		case ENOMEM:
			perror("Insufficent memory exist to initilize the mutex.");
			break;
		case EPERM:
			perror("Caller does not have the privilege to perform the operation.");
			break;
		case EINVAL:
			perror("The atributes object referenced by attr has the robust mutex attribute set without the process-shared attribute being set.");
			break;
		default:
			perror("Error pthread_mutex_init.");
			break;
	}
}

void lockMutexError() {
	switch(errno) {
		case EAGAIN:
			perror("The mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded.");
			break;
		case EINVAL:
			perror("The mutex was created with the protocol attribute having the value PTHREAD_PRIO_PROTECT and the calling threads priority is higher than the mutex's current priority ceiling.");
			break;
		case ENOTRECOVERABLE:
			perror("The state protected by the mutex is not recoverable.");
			break;
		case EOWNERDEAD:
			perror("The mutex is a robust mutex and the process containing the previous owning thread terminated while holding the mutex lock. The mutex lock shall be acquired by the calling thread and it is up to the new owner to make the state consistent.");
			break;
		case EDEADLK:
			perror("The mutex type is PTHREAD_MUTEX_ERRORCHECK and the current thread already owns the mutex or a deadlock condition was detected.");
			break;
		default:
			perror("Error pthread_mutex_lock.");
			break;
	}
}

void unlockMutexError() {
	switch(errno) {
		case EPERM:
			perror("The mutex type is PTHREAD_MUTEX_ERRORCHECK or PTHREAD_MUTEX_RECURSIVE, or the mutex is a robust mutex, and the current thread does not own the mutex.");
			break;
		default:
			perror("Error pthread_mutex_unlock.");
			break;
	}
}

void destroyMutexError() {
	switch(errno) {
		case EBUSY:
			perror("The implementation has detected an attempt to destroy the object referenced by mutex while it is locked or referenced.");
			break;
		case EINVAL:
			perror("The value specified by mutex is invalid.");
			break;
		default:
			perror("Error pthread_mutex_destroy.");
			break;
	}
}

// Variables de condicion
void initCondError() {
	switch(errno) {
		case EAGAIN:
			perror("The system lacked the necessary resources (other than memory) to initialise another condition variable.");
			break;
		case ENOMEM:
			perror("Insufficient memory exists to initialise the condition variable.");
			break;
		case EBUSY:
			perror("The implementation has detected an attempt to re-initialise the object referenced by cond, a previously initialised, but not yet destroyed, condition variable.");
			break;
		case EINVAL:
			perror("The value specified by attr is invalid.");
			break;
		default:
			perror("Error pthread_cond_init.");
			break;
	}
}

void waitCondError() {
	switch(errno) {
		case EINVAL:
			perror("The value specified by cond, mutex, or abstime is invalid or different mutexes were supplied for concurrent pthread_cond_wait() operations on the same condition variable or the mutex was not owned by the current thread at the time of the call.");
			break;
		default:
			perror("Error pthread_cond_wait.");
			break;
	}
}

void signalCondError() {
	switch(errno) {
		case EINVAL:
			perror("The value cond does not refer to an initialised condition variable.");
			break;
		default:
			perror("Error pthread_cond_signal.");
			break;
	}
}

void destroyCondError() {
	switch(errno) {
		case EBUSY:
			perror("The implementation has detected an attempt to destroy the object referenced by cond while it is referenced.");
			break;
		case EINVAL:
			perror("The value specified by cond is invalid.");
			break;
		default:
			perror("Error pthread_cond_destroy.");
			break;
	}
}
