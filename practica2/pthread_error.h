/*
* @file pthread_error.h
* @author Pablo Medina Suarez
*
* Declaracion de funciones para manejo de errores de las funciones
* de la libreria phtread.
*/
#ifndef PTHREAD_ERROR_H
#define PTHREAD_ERROR_H
// Threads
void createThreadError();
void joinThreadError();
// Mutex
void initMutexError();
void destroyMutexError();
void lockMutexError();
void unlockMutexError();
// Variables de condicion
void initCondError();
void destroyCondError();
void waitCondError();
void signalCondError();
#endif
