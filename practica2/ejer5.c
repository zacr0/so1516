/*
* @file ejer5.c
* @author Pablo Medina Suarez
*
* Programa que cuente las lineas de los ficheros de texto que le pasen como 
* parametros y al final muestre tambien el numero de lineas totales (contando
* las de todos los ficheros juntos).
* Debe crear un hilo por fichero obtenido por linea de argumentos, de forma
* que todos los ficheros se cuenten de manera paralela.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <pthread.h>		// Manejo de hilos

// FUNCIONES
int existeFichero(char *ruta_fichero);
void *contarLineasFichero(void *ruta_fichero);

int main(int argc, char **argv) {
	if (argc < 2) {
		// Numero de argumentos insuficiente
		perror("Formato incorrecto. Introduzca ./ejer5 fichero1 fichero2 ficheroN...\n");
		exit(EXIT_FAILURE);
	}

	int i, num_hilos = argc - 1;
	pthread_t threads[num_hilos];
	long total_lineas = 0, *aux_lineas;
	aux_lineas = calloc(1, sizeof(long));

	// Creacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_create(&(threads[i]), NULL, contarLineasFichero, (void *) argv[i+1])) {
			createThreadError();
		}
	}

	// Espera finalizacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], (void **) &aux_lineas)) {
			joinThreadError();
		}
		// Recogida sumas parciales
		total_lineas += *aux_lineas;
	}

	printf("Thread principal. Total de lineas = %ld.\n", total_lineas);
	free(aux_lineas);
	exit(EXIT_SUCCESS);
}

int existeFichero(char *ruta_fichero) {
    FILE *fich = NULL;
    int existe = 0;

    if ( (fich = fopen(ruta_fichero, "r")) != NULL) {
        existe = 1;
        fclose(fich);
    }

    return existe;
}

void *contarLineasFichero(void *ruta_fichero) {
	FILE *fich;
	char buffer[256], *ruta = (char *) ruta_fichero;
	long *num_lineas;

	// Reserva memoria variable devuelta
	if ((num_lineas = calloc(1, sizeof(long))) == NULL) {
		perror("Error calloc");
		exit(EXIT_FAILURE);
	}
	
	// Comprueba existencia fichero
	if (!existeFichero(ruta)) {
		fprintf(stderr, "El fichero %s no existe.\n", ruta);
		pthread_exit((void *) num_lineas);
	}

	// Apertura de fichero
	if ((fich = fopen(ruta, "r")) == NULL) {
		fprintf(stderr, "Fallo apertura del fichero %s.\n", ruta);
		exit(EXIT_FAILURE);
	}

	// Recorrido de fichero
	while (fgets(buffer, sizeof(buffer), fich)) {
		(*num_lineas)++;
	}

	printf("Thread %u. Fichero: %s. Lineas: %ld.\n", (unsigned int) pthread_self(), ruta, *num_lineas);

	fclose(fich);
	pthread_exit((void *) num_lineas);
}
