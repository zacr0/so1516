/*
* @file ejer2.c
* @author Pablo Medina Suarez
*
* Programa que cree N hebras, cada una crea 2 numeros aleatorios y guardara
* su suma en una variable que sera devuelta a la hebra llamadora.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <pthread.h>		// Manejo de hilos
#include <time.h>			// Generacion valores aleatorios

// FUNCIONES
void *sumAleatorios();

int main(int argc, char **argv) {
	if (argc != 2) {
		// Numero de argumentos insuficiente
		perror("Formato incorrecto. Introduzca ./ejer2 cantidad_de_hilos\n");
		exit(EXIT_FAILURE);
	}
	// Numero de argumentos suficiente
	int num_hilos = atoi(argv[1]), i, suma = 0;
	void *ret;
	long suma_parcial = 0;
	ret = &suma_parcial;

	if (num_hilos <= 0) {
		// Numero de hilos incorrecto
		perror("El numero de hilos debe ser un valor positivo.\n");
		exit(EXIT_FAILURE);
	}
	// Numero de hilos correcto
	pthread_t threads[num_hilos];
	
	// Creacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_create(&(threads[i]), NULL, (void *) sumAleatorios, NULL)) {
			createThreadError();
		}
	}

	// Recogida de sumas parciales
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], (void **) &ret)) {
			joinThreadError();
		}
		suma += *((long *) ret);
	}
	printf("Hilo principal. Suma total = %d.\n", suma);
	exit(EXIT_SUCCESS);
}


void *sumAleatorios() {
	long *suma, num1, num2;
	struct drand48_data randBuffer;
	// Semilla pseudoaleatoria (segundo del sistema y thread_id):
	srand48_r(time(NULL) * (long) pthread_self(), &randBuffer);

	// Reserva de memoria
	if ((suma = malloc(sizeof(long))) == NULL) {
		perror("Error malloc");
		exit(EXIT_FAILURE);
	}

	// Generacion de valores aleatorios 0-99
	lrand48_r(&randBuffer, &num1);
	lrand48_r(&randBuffer, &num2);
	// Reduccion a centenas
	num1 %= 100;
	num2 %= 100;
	*suma = num1 + num2;
	printf("Thread %u. Num1 = %ld. Num2 = %ld. Suma = %ld.\n", (unsigned int) pthread_self(), num1, num2, *suma);

	// Salida de la hebra y devolucion de suma
	printf("Thread %u finalizado.\n", (unsigned int) pthread_self());
	pthread_exit((void *) suma);
}
