/*
* @file ejer1a.c
* @author Pablo Medina Suarez
*
* Programa que crea dos hebras, cada una ejecutando una funcion que recibe 
* una cadena como parametro.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h> 			// I/O
#include <stdlib.h> 		// I/O
#include <pthread.h>		// Manejo de hilos
#include <unistd.h>			// POSIX

// FUNCIONES
void *mostrarCadena(char *str) {
	int i = 0;

	// Muestra str caracter a caracter
	while (str[i] != '\0') {
		putchar(str[i]);
		fflush(stdout);	// limpieza de buffer
		sleep(1);
		i++;
	}
	pthread_exit(NULL);
}

int main() {
	pthread_t th1, th2;
	char cad1[16] = "hola ";
	char cad2[16] = "mundo\n";

	// Creacion del primer hilo
	if (pthread_create(&th1, NULL, (void *) mostrarCadena, cad1)) {
		createThreadError();
	}
	if (pthread_join(th1, NULL)) {
		joinThreadError();
	}

	// Creacion del segundo hilo
	if (pthread_create(&th2, NULL, (void *) mostrarCadena, cad2)) {
		createThreadError();
	}
	if (pthread_join(th2, NULL)) {
		joinThreadError();
	}

	puts("Finalizacion de los hilos.");
	exit(EXIT_SUCCESS);
}
