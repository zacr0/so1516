/*
* @file ejer4_exec.c
* @author Pablo Medina Suarez
*
* Programa que de forma paralela convierta dos ficheros mp4 a mp3 con el
* programa ffmpeg. Pida el nombre de los dos ficheros por la linea de
* argumentos. Use alguna de las funciones exec() utilizadas anteriormente.
*
* Explicacion: en este ejercicio se ve como el funcionamiento de exec no es
* compatible con el uso de hilos en el proceso principal, pues al realizar
* la llamada al comando el proceso se vacia y carga el codigo de la
* nueva aplicacion, y por tanto todos los hilos creados se pierden.
*/
#include "pthread_error.h"	// Gestion de errores
#include <errno.h>			// Gestion de errores
#include <pthread.h>		// Manejo de hilos
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <string.h>			// strerr()
#include <unistd.h>			// exec()

// FUNCIONES
void *extraerAudioDeVideoExec(char *ruta_video);

int main(int argc, char **argv) {
	if (argc <= 1) {
		// Numero de argumentos insuficiente
		perror("Formato incorrecto. Introduzca ./ejer4 ruta_video_1 ruta_video_2 ruta_videoN\n");
		exit(EXIT_FAILURE);
	}
	// Numero de argumentos suficiente
	int i, num_hilos = argc -1;
	pthread_t threads[num_hilos];

	// Creacion de hilos (tantos como ficheros)
	for (i = 0; i < num_hilos; i++) {
		if (pthread_create(&threads[i], NULL, (void *) extraerAudioDeVideoExec, argv[i + 1])) {
			createThreadError();
		}
	}

	// Espera finalizacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], NULL)) {
			joinThreadError();
		}
	}

	puts("Thread principal finalizado.");
	exit(EXIT_SUCCESS);
}

void *extraerAudioDeVideoExec(char *ruta_video) {
	// Llamada a comando
	if (execlp("ffmpeg", "ffmpeg", "-i", ruta_video, "-f", "mp3", "-ab", "192000", "-ar", "48000", "-vn", "audio.mp3", NULL) == -1) {
		fprintf(stderr, "Error exec(): %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	pthread_exit(NULL);
}
