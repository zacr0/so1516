/*
* @file ejer6.c
* @author Pablo Medina Suarez
*
* Realice la multiplicacion en paralelo de una matriz de 3x3 por un vector de
* 3x1. Para ello cree tres hebras que repartan las filas de la matriz y del 
* vector. Cada hijo debe imprimir la fila que le ha tocado y el resultado final
* de la multiplicacion, la cual envia al padre. El padre debe esperar por la 
* terminacion de cada hijo y mostrar el vector resultante.
*
* // TODO ampliar a matriz / vector generico y mejorar limpieza del codigo
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <pthread.h>		// Manejo de hilos
#include <time.h>			// Generacion de valores aleatorios
#include <unistd.h>			// getpid()

// ESTRUCTURAS
typedef struct params {
	int indice;
	int *vector_fila, *vector_columna;
} params;

// FUNCIONES 
void *productoFilaColumna(void *params);
void mostrarVector(int *vector, int nElementos);
void mostrarMatriz(int matriz[][3], int nFil, int nCol);

int main() {
	pthread_t threads[3];
	srand(time(NULL) * getpid());
	int i, j, matriz[3][3], vector[3], resultado[3] = {0, 0, 0};
	params param[3];
	long *aux_suma;
	aux_suma = calloc(1, sizeof(long));

	// Inicializa matriz y vector con valores [0-9] aleatorios
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			matriz[i][j] = rand() % 10;
		}
	}
	for (i = 0; i < 3; i++){
		vector[i] = rand() % 10;
	}

	// Muestra matriz y vector creados
	puts("- Matriz inicializada:");
	mostrarMatriz(matriz, 3, 3);

	puts("- Vector inicializado:");
	mostrarVector(vector, 3);

	// Creacion de hilos
	for (i = 0; i < 3; i++) {
		// Actualiza referencias al comienzo de cada fila
		param[i].indice = i;
		param[i].vector_fila = matriz[i];
		param[i].vector_columna = vector;

		if (pthread_create(&(threads[i]), NULL, productoFilaColumna, &param[i])) {
			createThreadError();
		}
	}

	// Espera finalizacion de hilos
	for (i = 0; i < 3; i++) {
		if (pthread_join(threads[i], (void **) &aux_suma)) {
			joinThreadError();
		}
		// Recogida de productos resultantes
		resultado[i] = *aux_suma;
	}

	// Muestra vector resultado
	puts("Thread principal. Calculo finalizado.");
	puts("- Vector resultado: ");
	mostrarVector(resultado, 3);

	free(aux_suma);
	exit(EXIT_SUCCESS);
}

void *productoFilaColumna(void *param) {
	long *suma;
	int i;
	int *fila = ((params *) param)->vector_fila;
	int *columna = ((params *) param)->vector_columna;

	// Reserva de memoria de la variable devuelta al hilo inicial
	if ((suma = calloc(1, sizeof(long))) == NULL) {
		perror("Error calloc");
		exit(EXIT_FAILURE);
	}

	// Muestra fila asignada
	printf("Thread %u. Vector asignado: ", (unsigned int) pthread_self());
	mostrarVector(fila, 3);
	
	// Producto fila asignada por vector columna
	for (i = 0; i < 3; i++) {
		*suma += fila[i] * columna[i];
	}

	printf("Thread %u. Resultado = %ld.\n", (unsigned int) pthread_self(), *suma);
	pthread_exit((void *) suma);
}

void mostrarVector(int *vector, int nElementos) {
	int i;
	printf("[");
	for (i = 0; i < nElementos; i++) {
		printf("%d", *(vector + i));
		if (i < nElementos - 1) {
			printf(", ");
		}
	}
	printf("]\n");
}

void mostrarMatriz(int matriz[][3], int nFil, int nCol) {
	int i, j;
	for (i = 0; i < nFil; i++) {
		printf("[(");
		for (j = 0; j < nCol; j++) {
			printf("%d", matriz[i][j]);

			if (j < nCol - 1) {
				printf(", ");
			}
		}
		printf(")]\n");
	}
}
