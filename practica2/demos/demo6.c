#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#define NUM_PROCESOS 5

void * print_message (void *);

int main(void) 
{
    int error = 0;
    int i = 0;
    int j;
    int mess[NUM_PROCESOS];
    
    /* Creación de un Array de hebras */ 
    pthread_t thr [NUM_PROCESOS]; 
    
    for(i=0;i<NUM_PROCESOS;i++) 
		mess[i]=i+1;
    
    for(i = 0; i < NUM_PROCESOS; i++) 
	 {
        error = pthread_create( &(thr[i]), NULL, (void *) print_message, (void *) &mess[i]); 
        /* Manejar el error */
        //...
    }
    // NO se espera a los hilos (son detachable)
    
    printf("Soy el main()...\n"); 
    pthread_exit(NULL);
    printf("Esta linea no se ejecuta!!!\n");
}

void * print_message (void * ptr) 
{
    int error = 0;
    /* Desconexión del hilo cuando finalice. 
	 pthread_self() devuelve el ID de la hebra que invoca esta función.*/

    error = pthread_detach(pthread_self()); // indica que no debe esperarse
    /* Manejo del error si lo hubiera */
    //...

    printf("Soy la hebra: %d\n", *(int *) ptr);
    pthread_exit(NULL); 
}

