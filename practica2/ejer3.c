/*
* @file ejer3.c
* @author Pablo Medina Suarez
*
* Programa que realiza la suma en forma paralela de los valores de un 
* vector de 10 numeros enteros de 0-9. Utilice una cantidad de hilos 
* indicada como parametro de entrada por linea de argumentos y repartir 
* la suma del vector entre ellos. La suma debe ser el subtotal devuelto 
* por cada hilo.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O
#include <pthread.h>		// Manejo de hilos
#include <time.h>			// Generacion de valores aleatorios
#include <unistd.h>			// usleep()

// CONSTANTES
#define TAM 10				// tamaño del vector

// ESTRUCTURAS
typedef struct subvector {
	long *vector;	// porcion del vector asignado
	int num;		// numero de operaciones realizadas
} subvector;

// FUNCIONES
void *sumVector(void *v);

int main(int argc, char **argv) {
	if (argc != 2) {
		// Numero de argumentos insuficiente
		perror("Formato incorrecto. Introduzca ./ejer3 cantidad_de_hilos.\n");
		exit(EXIT_FAILURE);
	}

	// Numero de argumentos correcto
	int num_hilos = atoi(argv[1]);

	// Comprobacion numero de hilos
	if (num_hilos <= 0 || num_hilos > TAM) {
		fprintf(stderr, "El numero de hilos debe ser entre 1 y %d.\n", TAM);
		exit(EXIT_FAILURE);
	}
	// Numero de hilos correcto
	pthread_t threads[num_hilos];
	int i,
		operaciones_hilo = TAM / num_hilos, 
		resto = TAM % num_hilos;
	long numeros[TAM], 
		suma = 0, 
		suma_parcial = 0;
	subvector subvectores[num_hilos];
	void *ret;
	ret = &suma_parcial;
	// Semilla pseudoaleatoria
	struct drand48_data randBuffer;
	srand48_r(time(NULL) * getpid(), &randBuffer);

	// Inicializacion de vector (valores 0-9)
	printf("Vector: ");
	for (i = 0; i < TAM; i++) {
		lrand48_r(&randBuffer, &numeros[i]);
		numeros[i] %= TAM;
		printf("%ld ", numeros[i]);
	}
	printf("\n");

	// Creacion de hilos
	for (i = 0; i < num_hilos; i++) {
		// Posicion sobre el que operara el hilo
		subvectores[i].vector = &numeros[i * operaciones_hilo];

		if (i != (num_hilos - 1)) {
			/*
			* Si no es ultimo hilo le asigna el numero de 
			* operaciones correspondiente. Si lo es, le asigna
			* las operaciones restantes.
			*/
			subvectores[i].num = operaciones_hilo;
		} else {
			subvectores[i].num = operaciones_hilo + resto;
		}

		if (pthread_create(&(threads[i]), NULL, sumVector, (void *) &subvectores[i])) {
			createThreadError();
		}
	}

	// Recogida de sumas parciales
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], (void **) &ret)) {
			joinThreadError();
		}
		suma += *((long *) ret);
	}

	printf("Thread principal. Suma total = %ld.\n", suma);
	exit(EXIT_SUCCESS);
}

void *sumVector(void *v) {
	subvector *vec = v;
	int i,
		num_operaciones = vec->num;
	long *suma;

	// Reserva de memoria de la variable devuelta
	if ((suma = calloc(1, sizeof(long))) == NULL) {
		perror("Error calloc.\n");
		exit(EXIT_FAILURE);
	}

	// Suma de tantos elementos como valor tiene num_operaciones
	printf("Subvector: ");
	for (i = 0; i < num_operaciones; i++) {
		printf("%ld ", vec->vector[i]);
		*suma += vec->vector[i];
	}

	printf("Thread %u. Suma parcial = %ld.\n", (unsigned int) pthread_self(), *suma);
	pthread_exit((void *) suma);
}
