/*
* @file ejer1b.c
* @author Pablo Medina Suarez
*
* Programa que crea dos hebras que ejecutaran una funcion que recibe 
* una cadena como parametro, introducida por linea de comandos.
*/
#include "pthread_error.h"	// Gestion de errores
#include <stdio.h> 			// I/O
#include <stdlib.h> 		// I/O
#include <pthread.h>		// Manejo de hilos
#include <unistd.h>			// POSIX

// FUNCIONES
void *mostrarCadena(char *str) {
	int i = 0;

	// Muestra str caracter a caracter
	while (str[i] != '\0') {
		putchar(str[i]);
		fflush(stdout);	// limpieza de buffer
		sleep(1);
		i++;
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	if (argc != 3) {
		// Nº de argumentos insuficientes
		perror("Formato incorrecto. Introduzca ./ejer1b <cadena1> <cadena2>\n");
		exit(EXIT_FAILURE);
	}
	// Nº de argumentos correcto
	pthread_t th1, th2;

	// Creacion del primer hilo
	if (pthread_create(&th1, NULL, (void *) mostrarCadena, argv[1])) {
		createThreadError();
	}
	if (pthread_join(th1, NULL)) {
		joinThreadError();
	}

	// Creacion del segundo hilo
	if (pthread_create(&th2, NULL, (void *) mostrarCadena, argv[2])) {
		createThreadError();
	}
	if (pthread_join(th2, NULL)) {
		joinThreadError();
	}

	printf("\nFinalizacion de los hilos.\n");
	exit(EXIT_SUCCESS);
}
