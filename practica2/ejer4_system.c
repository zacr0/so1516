/*
* @file ejer4_system.c
* @author Pablo Medina Suarez
*
* Programa que de forma paralela convierta dos ficheros mp4 a mp3 con el
* programa ffmpeg. Pida el nombre de los dos ficheros por la linea de
* argumentos. Use la funcion system() para ello.
*/
#include "pthread_error.h"	// Gestion de errores
#include <pthread.h>		// Manejo de hilos
#include <stdio.h>			// I/O
#include <stdlib.h>			// I/O (system())

// FUNCIONES
void *extraerAudioDeVideo(char *ruta_video);

int main(int argc, char **argv) {
	if (argc <= 1) {
		// Numero de argumentos insuficiente
		perror("Formato incorrecto. Introduzca ./ejer4 ruta_video_1 ruta_video_2 ruta_videoN\n");
		exit(EXIT_FAILURE);
	}

	// Numero de argumentos suficiente
	int i, num_hilos = argc -1;
	pthread_t threads[num_hilos];

	// Creacion de hilos (tantos como ficheros)
	for (i = 0; i < num_hilos; i++) {
		if (pthread_create(&threads[i], NULL, (void *) extraerAudioDeVideo, argv[i + 1])) {
			createThreadError();
		}
	}

	// Espera finalizacion de hilos
	for (i = 0; i < num_hilos; i++) {
		if (pthread_join(threads[i], NULL)) {
			joinThreadError();
		}
	}

	puts("Thread principal finalizado.");
	exit(EXIT_SUCCESS);
}

void *extraerAudioDeVideo(char *ruta_video) {
	char command[256];
	int error;

	sprintf(command, "ffmpeg -i %s -f mp3 -ab 192000 -ar 48000 -vn %s_audio.mp3", (char *) ruta_video, (char *) ruta_video);
	//puts(command); 	// depuracion comando
	printf("Thread %u. Video = %s.\n", (unsigned int) pthread_self(), ruta_video);

	// Llamada a comando
	error = system(command);
	if (error == -1) {
		perror("Error in system() call.\n");
		exit(EXIT_FAILURE);
	} else if (error == 127) {
		perror("Error in execve call() \n");
		exit(EXIT_FAILURE);
	}

	pthread_exit(NULL);
}
